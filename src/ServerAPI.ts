import axiosInstance from "./axios";
import {
  ActiveCategories,
  RealEstateObjectTypeConfig,
  SearchLimitsConfig,
} from "./realEstateObjectInterfaces";

interface InitialStateConfig {
  currentList: RealEstateObjectTypeConfig[];
  totalLimits: SearchLimitsConfig;
}
interface AddfotoPostConfig {
  fileName: string;
  imageId: number;
}
interface DelImagePostConfig {
  imageName: string;
}

interface SortRealEstateConfig {
  minSquareUser: number;
  maxSquareUser: number;
  minPriceUser: number;
  maxPriceUser: number;
  activeCategories: ActiveCategories;
}

interface ResultSortConfig {
  sortList: RealEstateObjectTypeConfig[];
  searchLimits: SearchLimitsConfig;
}

export class ServerAPI {
  static sortRealEstateObjects(
    params: SortRealEstateConfig
  ): Promise<ResultSortConfig> {
    return axiosInstance
      .post<ResultSortConfig>("/sort", params)
      .then((result) => result.data);
  }

  static getInitialData(): Promise<InitialStateConfig> {
    return axiosInstance
      .get<InitialStateConfig>(`/initial`)
      .then((result) => result.data);
  }

  static addNewObjectRealEstate(
    objectInfo: RealEstateObjectTypeConfig
  ): Promise<InitialStateConfig> {
    return axiosInstance
      .post<InitialStateConfig>("/addObjectRealEstate", objectInfo)
      .then((result) => result.data);
  }

  static deleteObjectRealEstate(
    delRealEstateObjectId: number
  ): Promise<InitialStateConfig> {
    return axiosInstance
      .post<InitialStateConfig>("/deleteObjectRealEstate", {
        delRealEstateObjectId,
      })
      .then((result) => result.data);
  }

  static saveChangesObjectRealEstate(
    realEstateObject: RealEstateObjectTypeConfig
  ): Promise<InitialStateConfig> {
    return axiosInstance
      .post<InitialStateConfig>(
        "/saveChangesObjectRealEstate",
        realEstateObject
      )
      .then((result) => result.data);
  }
  static addImageObjectRealEstate(
    file: File,
    idRealEstate: number
  ): Promise<AddfotoPostConfig> {
    const fd = new FormData();
    fd.append("image", file, file.name);
    fd.append("realEstateId", `${idRealEstate}`);
    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };
    return axiosInstance
      .post<AddfotoPostConfig>("/upload", fd, config)
      .then((result) => result.data);
  }

  static deleteImage(id: number): Promise<DelImagePostConfig> {
    return axiosInstance
      .post<DelImagePostConfig>("/deleteImage", { imageId: id })
      .then((result) => result.data);
  }
  static editRealEstateObject(id: number): Promise<RealEstateObjectTypeConfig> {
    return axiosInstance
      .post<RealEstateObjectTypeConfig>("/editRealEstateObject", { id: id })
      .then((result) => result.data);
  }
  static showRealEstateObject(id: number): Promise<RealEstateObjectTypeConfig> {
    return axiosInstance
      .post<RealEstateObjectTypeConfig>("/showRealEstateObject", { id })
      .then((result) => result.data);
  }
}
