import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface StateConfig {
  images: string[];
}

const initialState: StateConfig = {
  images: [] as string[],
};

const addImagesSlice = createSlice({
  name: "images",
  initialState,
  reducers: {
    addImageToRealEstateObject(state, action: PayloadAction<string>) {
      const newStateImage = [...state.images];
      newStateImage.push(action.payload);
      state.images = newStateImage;
    },
    delImageToRealEstateObject(state, action: PayloadAction<string>) {
      const newStateImage = [...state.images];
      newStateImage.push(action.payload);
      state.images = newStateImage;
    },
  },
});

export const { addImageToRealEstateObject, delImageToRealEstateObject } =
  addImagesSlice.actions;

export default addImagesSlice.reducer;
