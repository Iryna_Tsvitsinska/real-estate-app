import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  ActiveCategories,
  RealEstateObjectTypeConfig,
  SearchLimitsConfig,
} from "../realEstateObjectInterfaces";
interface StateConfig {
  currentList: RealEstateObjectTypeConfig[];
  activeCategories: ActiveCategories;
  searchLimits: SearchLimitsConfig;
  totalLimits: SearchLimitsConfig;
  userLimits: SearchLimitsConfig;
  needRequestData: boolean;
}
interface initialStateFromServerConfig {
  currentList: RealEstateObjectTypeConfig[];
  totalLimits: SearchLimitsConfig;
}
const initialState: StateConfig = {
  currentList: [],
  searchLimits: {
    minPrice: 0,
    maxPrice: 0,
    minSquare: 0,
    maxSquare: 0,
  },
  totalLimits: {
    minPrice: 0,
    maxPrice: 0,
    minSquare: 0,
    maxSquare: 0,
  },
  userLimits: {
    minPrice: 0,
    maxPrice: 0,
    minSquare: 0,
    maxSquare: 0,
  },
  activeCategories: [],
  needRequestData: false,
};

const realEstateListSlice = createSlice({
  name: "realEstateList",
  initialState,
  reducers: {
    addActiveTypeRealEstate(state, action: PayloadAction<number>) {
      state.activeCategories = [...state.activeCategories, action.payload];
      state.needRequestData = true;
    },
    delActiveTypeRealEstate(state, action: PayloadAction<number>) {
      state.activeCategories = state.activeCategories.filter(
        (categoryIndex) => categoryIndex !== action.payload
      );
      state.needRequestData = true;
    },

    setMaxPriceUser(state, action: PayloadAction<number>) {
      state.userLimits.maxPrice = action.payload;
    },
    setMinPriceUser(state, action: PayloadAction<number>) {
      state.userLimits.minPrice = action.payload;
    },
    setMinSquareUser(state, action: PayloadAction<number>) {
      state.userLimits.minSquare = action.payload;
    },
    setMaxSquareUser(state, action: PayloadAction<number>) {
      state.userLimits.maxSquare = action.payload;
    },
    setRealEstateObjects(
      state,
      action: PayloadAction<RealEstateObjectTypeConfig[]>
    ) {
      state.currentList = [...action.payload];
    },
    setSearchLimits(state, action: PayloadAction<SearchLimitsConfig>) {
      state.searchLimits.minPrice = action.payload.minPrice;
      state.searchLimits.maxPrice = action.payload.maxPrice;
      state.searchLimits.minSquare = action.payload.minSquare;
      state.searchLimits.maxSquare = action.payload.maxSquare;
    },
    setInitialState(
      state,
      action: PayloadAction<initialStateFromServerConfig>
    ) {
      state.currentList = [...action.payload.currentList];

      state.searchLimits.minPrice = action.payload.totalLimits.minPrice;
      state.searchLimits.maxPrice = action.payload.totalLimits.maxPrice;
      state.searchLimits.minSquare = action.payload.totalLimits.minSquare;
      state.searchLimits.maxSquare = action.payload.totalLimits.maxSquare;

      state.totalLimits.minPrice = action.payload.totalLimits.minPrice;
      state.totalLimits.maxPrice = action.payload.totalLimits.maxPrice;
      state.totalLimits.minSquare = action.payload.totalLimits.minSquare;
      state.totalLimits.maxSquare = action.payload.totalLimits.maxSquare;

      state.userLimits.minPrice = action.payload.totalLimits.minPrice;
      state.userLimits.maxPrice = action.payload.totalLimits.maxPrice;
      state.userLimits.minSquare = action.payload.totalLimits.minSquare;
      state.userLimits.maxSquare = action.payload.totalLimits.maxSquare;
    },
    isNeedRequestData(state, action: PayloadAction<boolean>) {
      state.needRequestData = action.payload;
    },
  },
});

export const {
  addActiveTypeRealEstate,
  delActiveTypeRealEstate,
  setMinPriceUser,
  setMaxPriceUser,
  setMinSquareUser,
  setMaxSquareUser,
  setRealEstateObjects,
  setSearchLimits,
  setInitialState,
  isNeedRequestData,
} = realEstateListSlice.actions;

export default realEstateListSlice.reducer;
