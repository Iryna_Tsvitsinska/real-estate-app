import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface StateConfig {
  isFormAddNewObjectRealEstate: boolean;
}

const initialState: StateConfig = {
  isFormAddNewObjectRealEstate: false,
};

const AddObjectSlice = createSlice({
  name: "realEstateAddObject",
  initialState,
  reducers: {
    showAddForm(state, action: PayloadAction) {
      state.isFormAddNewObjectRealEstate = true;
    },
    hideAddForm(state, action: PayloadAction) {
      state.isFormAddNewObjectRealEstate = false;
    },
  },
});

export const { showAddForm, hideAddForm } = AddObjectSlice.actions;

export default AddObjectSlice.reducer;
