import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import realEstateListSlice from "./realEstateListSlice";
import editRealEstateObjectSlice from "./editObjectRealEstateSlice";
import addRealEstateObjectSlice from "./addRealEstateObjectSlice";
import showRealEstateObjectSlice from "./showRealEstateSlice";

const rootReduser = combineReducers({
  realEstateList: realEstateListSlice,
  editForm: editRealEstateObjectSlice,
  addForm: addRealEstateObjectSlice,
  showForm: showRealEstateObjectSlice,
});

export const store = configureStore({ reducer: rootReduser });

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();

export type RootState = ReturnType<typeof store.getState>;
