import { createSlice, PayloadAction } from "@reduxjs/toolkit";
interface StateConfig {
  isEditFormActive: boolean;
  editingRealEstateObjectId: number; //| null;
}

const initialState: StateConfig = {
  isEditFormActive: false,
  editingRealEstateObjectId: 0,
};

const editRealEstateObjectSlice = createSlice({
  name: "realEstateList",
  initialState,
  reducers: {
    showEditForm(state, action: PayloadAction<number>) {
      state.editingRealEstateObjectId = action.payload;
      state.isEditFormActive = true;
    },
    hideEditForm(state, action: PayloadAction) {
      state.isEditFormActive = false;
      state.editingRealEstateObjectId = 0;
    },
  },
});

export const { hideEditForm, showEditForm } = editRealEstateObjectSlice.actions;

export default editRealEstateObjectSlice.reducer;
