import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface ShowRealEstateConfig {
  showRealEstateId: number;
}

const initialState: ShowRealEstateConfig = {
  showRealEstateId: 0,
};

const ShowRealEstateSlice = createSlice({
  name: "showRealEstate",
  initialState,
  reducers: {
    showRealEstateObject(state, action: PayloadAction<number>) {
      state.showRealEstateId = action.payload;
    },
  },
});

export const { showRealEstateObject } = ShowRealEstateSlice.actions;

export default ShowRealEstateSlice.reducer;
