import { createSlice, PayloadAction } from "@reduxjs/toolkit";
// import {
//   ActiveCategories,
//   RealEstateObjectTypeConfig,
//   RealEstateType,
//   SearchLimitsConfig,
// } from "../realEstateObjectInterfaces";
// //  type ActiveCategories = RealEstateType[];
// interface StateConfig {
//   currentList: RealEstateObjectTypeConfig[];
//   activeCategories: ActiveCategories;
//   searchLimits: SearchLimitsConfig;
//   totalLimits: SearchLimitsConfig;
//   userLimits: SearchLimitsConfig;
//   isFormAddNewObjectRealEstate: boolean;
//   isEditFormActive: boolean;
//   editObjectRealEstate: RealEstateObjectTypeConfig | null;
// }
// interface initialStateFromServerConfig {
//   currentList: RealEstateObjectTypeConfig[];
//   totalLimits: SearchLimitsConfig;
// }
// const initialState: StateConfig = {
//   currentList: [],
//   searchLimits: {
//     minPrice: 0,
//     maxPrice: 0,
//     minSquare: 0,
//     maxSquare: 0,
//   },
//   totalLimits: {
//     minPrice: 0,
//     maxPrice: 0,
//     minSquare: 0,
//     maxSquare: 0,
//   },
//   userLimits: {
//     minPrice: 0,
//     maxPrice: 0,
//     minSquare: 0,
//     maxSquare: 0,
//   },

//   activeCategories: [],
//   isFormAddNewObjectRealEstate: false,
//   isEditFormActive: false,

//   editObjectRealEstate: null,
// };

// const realEstateSlice = createSlice({
//   name: "realEstateList",
//   initialState,
//   reducers: {
//     addActiveTypeRealEstate(state, action: PayloadAction<number>) {
//       state.activeCategories = [...state.activeCategories, action.payload];
//     },
//     delActiveTypeRealEstate(state, action: PayloadAction<number>) {
//       state.activeCategories = state.activeCategories.filter(
//         (categoryIndex) => categoryIndex !== action.payload
//       );
//     },

//     setMaxPriceUser(state, action: PayloadAction<number>) {
//       state.userLimits.maxPrice = action.payload;
//     },
//     setMinPriceUser(state, action: PayloadAction<number>) {
//       state.userLimits.minPrice = action.payload;
//     },
//     setMinSquareUser(state, action: PayloadAction<number>) {
//       state.userLimits.minSquare = action.payload;
//     },
//     setMaxSquareUser(state, action: PayloadAction<number>) {
//       state.userLimits.maxSquare = action.payload;
//     },
//     setRealEstateObjects(
//       state,
//       action: PayloadAction<RealEstateObjectTypeConfig[]>
//     ) {
//       state.currentList = [...action.payload];
//     },
//     setSearchLimits(state, action: PayloadAction<SearchLimitsConfig>) {
//       state.searchLimits.minPrice = action.payload.minPrice;
//       state.searchLimits.maxPrice = action.payload.maxPrice;
//       state.searchLimits.minSquare = action.payload.minSquare;
//       state.searchLimits.maxSquare = action.payload.maxSquare;
//     },
//     setInitialState(
//       state,
//       action: PayloadAction<initialStateFromServerConfig>
//     ) {
//       state.currentList = [...action.payload.currentList];

//       state.searchLimits.minPrice = action.payload.totalLimits.minPrice;
//       state.searchLimits.maxPrice = action.payload.totalLimits.maxPrice;
//       state.searchLimits.minSquare = action.payload.totalLimits.minSquare;
//       state.searchLimits.maxSquare = action.payload.totalLimits.maxSquare;

//       state.totalLimits.minPrice = action.payload.totalLimits.minPrice;
//       state.totalLimits.maxPrice = action.payload.totalLimits.maxPrice;
//       state.totalLimits.minSquare = action.payload.totalLimits.minSquare;
//       state.totalLimits.maxSquare = action.payload.totalLimits.maxSquare;

//       state.userLimits.minPrice = action.payload.totalLimits.minPrice;
//       state.userLimits.maxPrice = action.payload.totalLimits.maxPrice;
//       state.userLimits.minSquare = action.payload.totalLimits.minSquare;
//       state.userLimits.maxSquare = action.payload.totalLimits.maxSquare;
//     },

//     showAddForm(state, action: PayloadAction) {
//       state.isFormAddNewObjectRealEstate = true;
//     },
//     hideAddForm(state, action: PayloadAction) {
//       state.isFormAddNewObjectRealEstate = false;
//     },
//     showEditForm(state, action: PayloadAction<RealEstateObjectTypeConfig>) {
//       state.editObjectRealEstate = action.payload;
//       state.isEditFormActive = true;
//     },
//     hideEditForm(state, action: PayloadAction) {
//       state.isEditFormActive = false;
//       state.editObjectRealEstate = null;
//     },
//   },
// });

// export const {
//   addActiveTypeRealEstate,
//   delActiveTypeRealEstate,
//   setMinPriceUser,
//   setMaxPriceUser,
//   setMinSquareUser,
//   setMaxSquareUser,
//   setRealEstateObjects,
//   setSearchLimits,
//   showAddForm,
//   hideAddForm,
//   setInitialState,
//   hideEditForm,
//   showEditForm,

//   // setCurrenteditingObjectId,
// } = realEstateSlice.actions;

// export default realEstateSlice.reducer;
