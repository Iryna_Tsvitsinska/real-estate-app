import axios from 'axios';
// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

export const ServerURL = 'http://localhost:7000';

export default axios.create({
  baseURL: ServerURL,
  headers: {
    'Content-type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});
