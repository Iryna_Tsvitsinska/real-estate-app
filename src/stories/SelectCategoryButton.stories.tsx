import "bulma";
import "../scss/fontawesome/css/all.css";

import { ComponentMeta, ComponentStory } from "@storybook/react";
import { SelectCategoryButton } from "../components/SelectCategoryButton";

export default {
  title: "Select Category Button",
  component: SelectCategoryButton,
  argTypes: {
    label: { control: "text" },
    type: {
      control: {
        type: "inline-radio",
      },
    },
  },
} as ComponentMeta<typeof SelectCategoryButton>;

const Template: ComponentStory<typeof SelectCategoryButton> = (args) => (
  <div className="m-5">
    <SelectCategoryButton {...args} />
  </div>
);

export const Office = Template.bind({});

Office.args = {
  label: "Office",
  type: "OFFICE",
  isSelected: false,
};
export const Garage = Template.bind({});
Garage.args = {
  label: "Garage",
  type: "GARAGE",
  isSelected: false,
};
export const Warehouse = Template.bind({});
Warehouse.args = {
  label: "Warehouse",
  type: "WAREHOUSE",
  isSelected: false,
};
export const Flat = Template.bind({});
Flat.args = {
  label: "Flat",
  type: "FLAT",
  isSelected: true,
};
export const House = Template.bind({});
House.args = {
  label: "House",
  type: "HOUSE",
  isSelected: false,
};
