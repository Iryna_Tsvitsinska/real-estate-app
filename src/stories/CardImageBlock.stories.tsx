import React from "react";
import "bulma";

import { ComponentMeta, ComponentStory } from "@storybook/react";
import { CardImagesBlock } from "../components/image/CardImagesBlock";

export default {
  title: "Card Image Block",
  component: CardImagesBlock,
  argTypes: {
    images: { control: "object" },
    className: {
      options: ["midle-size-image", "large-size-image", "logo-size"],
      control: "radio",
    },
  },
} as ComponentMeta<typeof CardImagesBlock>;

const Template: ComponentStory<typeof CardImagesBlock> = (args) => (
  <div style={{ height: "600px" }}>
    <CardImagesBlock {...args} />
  </div>
);

export const Images = Template.bind({});

Images.args = {
  images: [
    { fileName: "1637579258042.jpg", imageId: 6 },
    { fileName: "1637854259473.jpg", imageId: 20 },
    { fileName: "1637854263937.jpg", imageId: 21 },
  ],
  className: "logo-size",
};
export const ImagesSmall = Template.bind({});
ImagesSmall.args = {
  images: [
    { fileName: "1637579258042.jpg", imageId: 6 },
    { fileName: "1637854259473.jpg", imageId: 20 },
    // { fileName: "1637854263937.jpg", imageId: 21 },
  ],
  className: "logo-size",
};
export const ImagesLarge = Template.bind({});
ImagesLarge.args = {
  images: [
    { fileName: "1637579258042.jpg", imageId: 6 },
    { fileName: "1637854259473.jpg", imageId: 20 },
    { fileName: "1637854263937.jpg", imageId: 21 },
  ],
  className: "large-size-image",
};
export const ImagesEmpty = Template.bind({});
ImagesEmpty.args = {
  images: [],
  className: "midle-size-image",
};
