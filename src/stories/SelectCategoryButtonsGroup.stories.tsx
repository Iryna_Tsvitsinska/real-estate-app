import "bulma";
import "../scss/fontawesome/css/all.css";
import "../../src/scss/SelectCategoryButton.scss";

import { ComponentMeta, ComponentStory } from "@storybook/react";
import { SelectCategoryButtonsGroup } from "../components/SelectCategoryButtonsGroup";

const types = { GARAGE: 0, WAREHOUSE: 1, OFFICE: 2, FLAT: 3, HOUSE: 4 };

export default {
  title: "Select Category Buttons Group",
  component: SelectCategoryButtonsGroup,
  argTypes: {
    activeCategories: {
      options: Object.values(types),
      // options: [0, 1, 2, 3, 4],
      mapping: types,
      control: {
        type: "multi-select",
        labels: {
          0: "Гараж",
          1: "Склад",
          2: "Офис",
          3: "Квартира",
          4: "Дом",
        },
      },
    },
    buttonsId: {
      control: "object",
    },
  },
} as ComponentMeta<typeof SelectCategoryButtonsGroup>;

const Template: ComponentStory<typeof SelectCategoryButtonsGroup> = (args) => (
  <SelectCategoryButtonsGroup {...args} />
);

export const Group = Template.bind({});

Group.args = {
  buttonsId: [0, 1, 2, 3, 4],
  activeCategories: [1, 2],
};
