import { ComponentMeta, ComponentStory } from "@storybook/react";
import { DropDownList } from "../components/DropDownList";

export default {
  title: "Drop Down List",
  component: DropDownList,
  argTypes: {
    title: { control: "text" },
    // list: { control: "object" },
  },
  parameters: {
    title: "Test",
  },
} as ComponentMeta<typeof DropDownList>;

const Template: ComponentStory<typeof DropDownList> = (args) => (
  <div className="box">
    <DropDownList {...args} />
  </div>
);

export const SimpleList = Template.bind({});

SimpleList.args = {
  list: [
    {
      name: "cat",
      value: 0,
      isSelected: true,
    },
    {
      name: "dog",
      value: 1,
      isSelected: false,
    },
    {
      name: "mouse",
      value: 2,
      isSelected: false,
    },
  ],
  title: "Simple list",
  setValue: () => {},
};
