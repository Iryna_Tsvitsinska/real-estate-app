import React from "react";
import "bulma";
import "../scss/fontawesome/css/all.css";
import "../../src/scss/RangeSelector.scss";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import { RangeSelector } from "../components/RangeSelector";

export default {
  title: "Range Selector",
  component: RangeSelector,
  argTypes: {
    min: { control: "number" },

    max: {
      control: { type: "range", min: 400, max: 1200, step: 50 },
    },
    label: { control: "text" },
    showInputs: {
      control: "boolean",
      description: "Toggle min/max text fields visibility",
    },
  },
  arg: {
    showInputs: true,
  },
} as ComponentMeta<typeof RangeSelector>;

const Template: ComponentStory<typeof RangeSelector> = (args) => (
  <div className="box">
    <RangeSelector {...args} />
  </div>
);

export const Price = Template.bind({});

Price.args = {
  min: 20,
  max: 4000,
  label: "цена",
};

export const SquareMeters = Template.bind({});

SquareMeters.args = {
  min: 120,
  max: 300,
  label: "квадратные метры",
};
