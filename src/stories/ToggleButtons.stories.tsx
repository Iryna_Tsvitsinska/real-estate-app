import React from 'react';
import "bulma";
import "../index.css";
import "../scss/fontawesome/css/all.css";

import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ToggleButtons } from '../components/ToggleButtonsComponent';

export default {
  title: 'ToggleButtons',
  component: ToggleButtons,
  argTypes:{
      description: {control:"text"},
      yesTitle: {control:"text"},
      noTitle: {control:"text"},
      initialValue : {control: "boolean"}
  }
} as ComponentMeta<typeof ToggleButtons>;

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof ToggleButtons> = (args) => <ToggleButtons {...args} />;

export const FirstStory = Template.bind({});

FirstStory.args = {
    description : "Do you have a dog?",
    yesTitle : "yes",
    noTitle : "no",
    initialValue: false
};