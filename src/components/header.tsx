import { NavLink, useNavigate } from "react-router-dom";
import { showAddForm } from "../redux/addRealEstateObjectSlice";
import { useAppDispatch } from "../redux/store";

export const HeaderRealEstate = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-menu">
        <div className="navbar-start">
          <div className="navbar-item has-dropdown is-hoverable"></div>
          <button className="button navbar-item" onClick={() => navigate("/")}>
            Главная
          </button>
          <button
            className="button navbar-item"
            onClick={() => dispatch(showAddForm())}
          >
            <NavLink to="addNewRealEstateObject">+ добавить обьект</NavLink>
          </button>
        </div>
      </div>
    </nav>
  );
};
