import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import {
  FlatConfig,
  GarageConfig,
  HouseConfig,
  OfficeConfig,
  RealEstateObjectTypeConfig,
  RealEstateType,
  WarehouseConfig,
} from "../../realEstateObjectInterfaces";
import { ServerAPI } from "../../ServerAPI";
import { CardImagesBlock } from "../image/CardImagesBlock";
import { ShowFlatFiedls } from "./ShowFlatFields";
import { ShowGarageFiedls } from "./ShowGarageFields";
import { ShowHouseFiedls } from "./ShowHouseFields";
import { ShowOfficeFiels } from "./ShowOfficeFields";
import { ShowWarehouseFiedls } from "./ShowWarehouseFields";

export const ShowRealEstateObject = () => {
  const { id } = useParams();
  const showRealEstateId = Number(id);

  const navigate = useNavigate();
  const [realEstateObject, setRealEstateObject] =
    useState<RealEstateObjectTypeConfig>();

  useEffect(() => {
    if (showRealEstateId) {
      ServerAPI.showRealEstateObject(showRealEstateId).then((data) => {
        setRealEstateObject(data);
      });
    } else {
      navigate("/", { replace: true });
    }
  }, [showRealEstateId, navigate]);

  if (!realEstateObject) {
    return <div>1</div>;
  }

  return (
    <div className="block">
      <div className="box">
        <div className="box images">
          <CardImagesBlock
            images={realEstateObject.images}
            className="midle-size-image"
          />
        </div>
        {realEstateObject.propertyType === RealEstateType.FLAT && (
          <ShowFlatFiedls realEstateObject={realEstateObject as FlatConfig} />
        )}
        {realEstateObject.propertyType === RealEstateType.OFFICE && (
          <ShowOfficeFiels
            realEstateObject={realEstateObject as OfficeConfig}
          />
        )}
        {realEstateObject.propertyType === RealEstateType.GARAGE && (
          <ShowGarageFiedls
            realEstateObject={realEstateObject as GarageConfig}
          />
        )}
        {realEstateObject.propertyType === RealEstateType.WAREHOUSE && (
          <ShowWarehouseFiedls
            realEstateObject={realEstateObject as WarehouseConfig}
          />
        )}
        {realEstateObject.propertyType === RealEstateType.HOUSE && (
          <ShowHouseFiedls realEstateObject={realEstateObject as HouseConfig} />
        )}
      </div>
    </div>
  );
};
