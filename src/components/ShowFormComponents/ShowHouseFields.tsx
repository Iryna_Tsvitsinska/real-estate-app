import { HouseConfig } from "../../realEstateObjectInterfaces";
import {
  ElectricityTypeMap,
  OwnershipTypeMap,
  SewerageTypeMap,
  WatterTypeMap,
} from "../PropertyTypeMap";

interface ShowHouseFielsConfig {
  realEstateObject: HouseConfig;
}

export const ShowHouseFiedls = (props: ShowHouseFielsConfig) => {
  const {
    fullSquare,
    activeSquare,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    numberOfStoreys,
    price,
    hasElevator,
    ownershipType,
    propertyState,
    // id,
    description,
    gardenSquare,
  } = props.realEstateObject;
  return (
    <div className="block">
      <div className="title">
        {propertyState === 0 ? "Сдается дом" : "Продажа дома"}{" "}
      </div>

      <div className="columns">
        <div className="column subtitle">{`Цена: ${price}$`}</div>
        <div className="column subtitle">
          {`Площа:  ${fullSquare} / ${activeSquare} м/кв`}
        </div>
        <div className="column subtitle">
          {`Этажность :  ${numberOfStoreys}`}
        </div>
        <div className="column subtitle">
          {`Площадь сада :  ${gardenSquare}`}
        </div>
      </div>
      <div className="is-buttons">
        {hasElevator && (
          <button className="button is-primary is-outlined is-rounded">
            Лифт
          </button>
        )}
        {heatingType === 0 && (
          <button className="button is-primary is-outlined is-rounded">
            Собственное отопление
          </button>
        )}
        {waterType === 0 && (
          <button className="button is-primary is-outlined is-rounded">
            Скважина
          </button>
        )}
      </div>
      <div className=" mt-3 columns">
        <div className="column subtitle">
          {`Право собственности:  ${OwnershipTypeMap[ownershipType]}`}
        </div>
        <div className="column subtitle">
          {`Електричество:  ${ElectricityTypeMap[electricityType]}`}
        </div>
      </div>
      <div className="columns">
        <div className="column subtitle">
          {`Вода:  ${WatterTypeMap[waterType]}`}
        </div>
        <div className="column subtitle">
          {`Канализация:  ${SewerageTypeMap[sewerageType]}`}
        </div>
      </div>

      {description.length > 0 && (
        <div>
          <div className="subtitle">Описание</div>
          <div className="notification">{description}</div>
        </div>
      )}
    </div>
  );
};
// NONE = -1,
//   OWN = 0,
//   CENTRALIZED = 1,
