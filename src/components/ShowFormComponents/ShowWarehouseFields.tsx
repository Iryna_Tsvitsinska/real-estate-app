import { WarehouseConfig } from "../../realEstateObjectInterfaces";
import {
  ElectricityTypeMap,
  OwnershipTypeMap,
  SewerageTypeMap,
  WatterTypeMap,
} from "../PropertyTypeMap";

interface ShowWarehouseFielsConfig {
  realEstateObject: WarehouseConfig;
}

export const ShowWarehouseFiedls = (props: ShowWarehouseFielsConfig) => {
  const {
    fullSquare,
    activeSquare,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    price,
    hasElevator,
    ownershipType,
    propertyState,
    // id,
    description,
    hasSecurity,
  } = props.realEstateObject;
  return (
    <div className="block">
      <div className="title">
        {propertyState === 0 ? "Сдается склад" : "Продажа склада"}{" "}
      </div>

      <div className="columns">
        <div className="column subtitle">{`Цена: ${price}$`}</div>
        <div className="column subtitle">
          {`Площа:  ${fullSquare} / ${activeSquare}`}
        </div>
        <div className="column subtitle">
          {`Этаж :  ${floor} из ${totalBuildingStoreys}`}
        </div>
      </div>
      <div className="is-buttons">
        {hasSecurity && (
          <button className="button is-primary is-outlined is-rounded">
            С балконом
          </button>
        )}
        {hasElevator && (
          <button className="button is-primary is-outlined is-rounded">
            Лифт
          </button>
        )}
        {heatingType === 0 && (
          <button className="button is-primary is-outlined is-rounded">
            Собственное отопление
          </button>
        )}
        {waterType === 0 && (
          <button className="button is-primary is-outlined is-rounded">
            Скважина
          </button>
        )}
        {numberOfStoreys > 1 && (
          <button className="button is-primary is-outlined is-rounded">
            Многоуровневая
          </button>
        )}
      </div>
      <div className=" mt-3 columns">
        <div className="column subtitle">
          {`Право собственности:  ${OwnershipTypeMap[ownershipType]}`}
        </div>
        <div className="column subtitle">
          {`Електричество:  ${ElectricityTypeMap[electricityType]}`}
        </div>
      </div>
      <div className="columns">
        <div className="column subtitle">
          {`Вода:  ${WatterTypeMap[waterType]}`}
        </div>
        <div className="column subtitle">
          {`Канализация:  ${SewerageTypeMap[sewerageType]}`}
        </div>
      </div>

      {description.length > 0 && (
        <div>
          <div className="subtitle">Описание</div>
          <div className="notification">{description}</div>
        </div>
      )}
    </div>
  );
};
// NONE = -1,
//   OWN = 0,
//   CENTRALIZED = 1,
