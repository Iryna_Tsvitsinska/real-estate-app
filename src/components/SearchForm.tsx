import React from "react";
import { connect } from "react-redux";
import {
  ActiveCategories,
  RealEstateType,
  SearchLimitsConfig,
} from "../realEstateObjectInterfaces";
import {
  addActiveTypeRealEstate,
  delActiveTypeRealEstate,
  isNeedRequestData,
  setMaxPriceUser,
  setMaxSquareUser,
  setMinPriceUser,
  setMinSquareUser,
} from "../redux/realEstateListSlice";
import { RootState, useAppDispatch } from "../redux/store";

import { RangeSelector } from "./RangeSelector";
import { SelectCategoryButtonsGroup } from "./SelectCategoryButtonsGroup";

type SeachFormPropsConfig = {
  activeCategories: ActiveCategories;
  searchLimits: SearchLimitsConfig;

  minPriceUser: number;
  maxPriceUser: number;
  minSquareUser: number;
  maxSquareUser: number;
};

const buttonsId = [0, 1, 2, 3, 4];

const SearchForm = (props: SeachFormPropsConfig) => {
  const { activeCategories, searchLimits } = props;
  const { minPrice, maxPrice, minSquare, maxSquare } = searchLimits;

  const dispatch = useAppDispatch();
  let timeoutId: ReturnType<typeof setTimeout>;

  const resetTimer = () => {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => {
      dispatch(isNeedRequestData(true));
    }, 500);
  };

  const handleChangeMinPrice = (value: number) => {
    minPrice > value
      ? dispatch(setMinPriceUser(minPrice))
      : dispatch(setMinPriceUser(value));
    resetTimer();
  };

  const handleChangeMaxPrice = (value: number) => {
    maxPrice < value
      ? dispatch(setMaxPriceUser(maxPrice))
      : dispatch(setMaxPriceUser(value));
    resetTimer();
  };
  const handleChangeMinSquare = (value: number) => {
    minSquare > value
      ? dispatch(setMinSquareUser(minSquare))
      : dispatch(setMinSquareUser(value));
    resetTimer();
  };
  const handleChangeMaxSquare = (value: number) => {
    maxSquare < value
      ? dispatch(setMaxSquareUser(maxSquare))
      : dispatch(setMaxSquareUser(value));
    resetTimer();
  };

  const toggleActiveTypeRealEstate = (
    typeIndex: RealEstateType,
    isSelected: boolean
  ) => {
    !isSelected
      ? dispatch(delActiveTypeRealEstate(typeIndex))
      : dispatch(addActiveTypeRealEstate(typeIndex));
  };

  return (
    <div className="box">
      <SelectCategoryButtonsGroup
        activeCategories={activeCategories}
        buttonsId={buttonsId}
        onChange={toggleActiveTypeRealEstate}
      />

      <div className="columns">
        <div className="m-2 column field-seach">
          {maxPrice > 0 && (
            <RangeSelector
              label={"Цена"}
              min={minPrice}
              onMinChange={handleChangeMinPrice}
              max={maxPrice}
              onMaxChange={handleChangeMaxPrice}
            />
          )}
        </div>
        <div className="m-2 column field-seach">
          {maxPrice > 0 && (
            <RangeSelector
              label={"Площадь"}
              min={minSquare}
              onMinChange={handleChangeMinSquare}
              max={maxSquare}
              onMaxChange={handleChangeMaxSquare}
            />
          )}
        </div>
      </div>
    </div>
  );
};

const connector = connect((state: RootState) => ({
  activeCategories: state.realEstateList.activeCategories,
  searchLimits: state.realEstateList.searchLimits,
  minPriceUser: state.realEstateList.userLimits.minPrice,
  maxPriceUser: state.realEstateList.userLimits.maxPrice,
  minSquareUser: state.realEstateList.userLimits.minSquare,
  maxSquareUser: state.realEstateList.userLimits.maxSquare,
}));

export default connector(SearchForm);
