import { useEffect, useState } from "react";

type ToggleButtonsConfig = {
  description: string;
  yesTitle: string;
  noTitle: string;
  initialValue: boolean;
  onToggle(active: boolean): void;
};

export const ToggleButtons = (props: ToggleButtonsConfig) => {
  const { description, initialValue, onToggle, yesTitle, noTitle } = props;

  const [isActive, setValue] = useState(initialValue);

  const setValueHandler = () => {
    setValue((prev) => !prev);
    onToggle(isActive);
  };

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  return (
    <div className="is-buttons">
      <label className=" m-3 label buttons-discription">{description} </label>
      <button
        className={`button ${isActive === false && "is-primary"} `}
        onClick={() => setValueHandler()}
      >
        {noTitle}
      </button>
      <button
        className={`button ${isActive === true && "is-primary"} `}
        onClick={() => setValueHandler()}
      >
        {yesTitle}
      </button>
    </div>
  );
};
