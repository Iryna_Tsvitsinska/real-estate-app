import React from "react";
import "bulma";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import Provider from "../redux/Provider";
import { store } from "../redux/store";
import { FlatCard } from "./cards/FlatCard";

export default {
  title: "Flat Card",
  component: FlatCard,
  argTypes: {
    flat: { control: "object" },
  },
} as ComponentMeta<typeof FlatCard>;

const Template: ComponentStory<typeof FlatCard> = (args) => (
  <Provider store={store} children={<FlatCard {...args} />} />
);

export const Flat = Template.bind({});

Flat.args = {
  flat: {
    activeSquare: 53,
    description:
      "lrfetr44 er e\nrerere \nrerer\nre\nre\nrw\nre\nwr\nrgtrf rrtrt rtr re elprpero[e pe rpero [perp pe rop orer et er  etet t rt rt truy t werfew  4 e e e rer er  lp[ol[pwoieoiwuhewioqakdlasmcfjfbxfjds rwedlw;hfjkswgfu;wh rf;egl[r epoiwhifewwgerhyre",
    electricityType: 1,
    floor: 3,
    fullSquare: 100,
    gasType: 1,
    hasBalcon: true,
    hasElevator: true,
    heatingType: 0,
    id: 1,
    images: [
      { fileName: "1637579258042.jpg", imageId: 6 },
      { fileName: "1637854259473.jpg", imageId: 20 },
      { fileName: "1637854263937.jpg", imageId: 21 },
    ],
    numberOfStoreys: 1,
    ownershipType: 0,
    price: 52000,
    propertyState: 1,
    propertyType: 3,
    sewerageType: 1,
    totalBuildingStoreys: 9,
    waterType: 1,
  },
};
