import { useState } from "react";
import { OfficeConfig } from "../../realEstateObjectInterfaces";

import { BasicFieldsComponent } from "./BasicFieldComponent";

interface OfficeFieldsComponentConfig {
  setProperty(name: keyof OfficeConfig, value: number | boolean): void;
  objectRealEstate: OfficeConfig;
}

export const OfficeFieldsComponent = (props: OfficeFieldsComponentConfig) => {
  const { setProperty, objectRealEstate } = props;
  const [activeSquare, setActiveSquare] = useState(
    objectRealEstate.activeSquare
  );
  const [hasParkingOffice, setParkinfInfo] = useState(false);
  const toggleHasParking = () => {
    setProperty("hasParking", !hasParkingOffice);
    setParkinfInfo((prev) => !prev);
  };

  const {
    gasType,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    hasElevator,
    ownershipType,
  } = objectRealEstate;

  const initialBasicProperties = {
    gasType,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    hasElevator,
    ownershipType,
    activeSquare,
  };

  const onChangeActiveSquareHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = Number(e.target.value);
    setActiveSquare(value);
    setProperty("activeSquare", value);
  };

  return (
    <div className="block">
      <div className="column">
        <label className="label">Активная площадь</label>
        <input
          className="input"
          type="number"
          name="activeSquare"
          placeholder="0"
          value={activeSquare}
          onChange={onChangeActiveSquareHandler}
        />
      </div>
      <BasicFieldsComponent
        setProperty={setProperty}
        initialProperties={initialBasicProperties}
      />
      <div className="is-buttons">
        <label className=" m-3 label buttons-discription">
          {"Наличие парковки"}{" "}
        </label>
        <button
          className={`button ${!hasParkingOffice && "is-button-active"} `}
          onClick={() => toggleHasParking()}
        >
          Нет
        </button>
        <button
          className={`button ${hasParkingOffice && "is-button-active"} `}
          onClick={() => toggleHasParking()}
        >
          Есть
        </button>
      </div>
    </div>
  );
};
