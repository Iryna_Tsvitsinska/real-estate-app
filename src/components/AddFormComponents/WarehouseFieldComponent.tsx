import { useState } from "react";
import { WarehouseConfig } from "../../realEstateObjectInterfaces";

import { BasicFieldsComponent } from "./BasicFieldComponent";

interface WarehouseFieldsComponentConfig {
  setProperty(name: keyof WarehouseConfig, value: number | boolean): void;
  objectRealEstate: WarehouseConfig;
}

export const WarehouseFieldsComponent = (
  props: WarehouseFieldsComponentConfig
) => {
  const { setProperty, objectRealEstate } = props;
  const [activeSquare, setActiveSquare] = useState(
    objectRealEstate.activeSquare
  );
  const [hasSecurityWarehouse, setSecurityWarehouse] = useState(false);
  const toggleHasSecurity = () => {
    setProperty("hasSecurity", !hasSecurityWarehouse);
    setSecurityWarehouse((prev) => !prev);
  };

  const {
    gasType,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    hasElevator,
    ownershipType,
  } = objectRealEstate;

  const initialBasicProperties = {
    gasType,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    hasElevator,
    ownershipType,
    activeSquare,
  };

  const onChangeActiveSquareHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = Number(e.target.value);
    setActiveSquare(value);
    setProperty("activeSquare", value);
  };

  return (
    <div className="block">
      <div className="column">
        <label className="label">Активная площадь</label>
        <input
          className="input"
          type="number"
          name="activeSquare"
          placeholder="0"
          value={activeSquare}
          onChange={onChangeActiveSquareHandler}
        />
      </div>
      <BasicFieldsComponent
        setProperty={setProperty}
        initialProperties={initialBasicProperties}
      />

      <div className="is-buttons">
        <label className=" m-3 label buttons-discription">
          {"Наличие охраны"}{" "}
        </label>
        <button
          className={`button ${!hasSecurityWarehouse && "is-button-active"} `}
          onClick={() => toggleHasSecurity()}
        >
          Нет
        </button>
        <button
          className={`button ${hasSecurityWarehouse && "is-button-active"} `}
          onClick={() => toggleHasSecurity()}
        >
          Есть
        </button>
      </div>
    </div>
  );
};
