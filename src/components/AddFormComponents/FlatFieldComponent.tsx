import { useState } from "react";
import { FlatConfig } from "../../realEstateObjectInterfaces";
import { DropDownList } from "../DropDownList";

import {
  createParamsList,
  ElectricityTypeMap,
  floorTypeMap,
  HeatingTypeMap,
  SewerageTypeMap,
  sortArrObj,
  totalBuildingStoreysMap,
  WatterTypeMap,
} from "../PropertyTypeMap";

interface FlatFieldsComponentConfig {
  setProperty(name: keyof FlatConfig, value: number | boolean): void;
  objectRealEstate: FlatConfig;
}

export const FlatFieldsComponent = (props: FlatFieldsComponentConfig) => {
  const { setProperty, objectRealEstate } = props;

  const initialInfo = objectRealEstate;

  const [hasElevator, setHasElevator] = useState(initialInfo.hasElevator);
  const [activeSquare, setActiveSquare] = useState(initialInfo.activeSquare);

  const listWatter = createParamsList(WatterTypeMap, initialInfo.waterType);

  const listSewerage = createParamsList(
    SewerageTypeMap,
    initialInfo.sewerageType
  );
  const listHeating = createParamsList(HeatingTypeMap, initialInfo.heatingType);
  const listElectricity = createParamsList(
    ElectricityTypeMap,
    initialInfo.electricityType
  );

  const floorMap = createParamsList(floorTypeMap, initialInfo.floor);
  const listTotalBuildingStoreysMap = createParamsList(
    totalBuildingStoreysMap,
    initialInfo.totalBuildingStoreys
  );

  sortArrObj(floorMap);

  const toggleHasElevator = () => {
    setProperty("hasElevator", !hasElevator);
    setHasElevator((prev) => !prev);
  };
  const onChangeActiveSquareHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = Number(e.target.value);
    setActiveSquare(value);
    setProperty("activeSquare", value);
  };

  return (
    <div className="block">
      <div className="columns">
        <div className="column">
          <label className="label">Жилая площадь</label>
          <input
            className="input"
            type="number"
            name="activeSquare"
            placeholder="0"
            value={activeSquare}
            onChange={onChangeActiveSquareHandler}
          />
        </div>
        <div className="column">
          <label className="label">Площадь кухни</label>
          <input
            className="input"
            placeholder="0"
            name="KitchenSquare"
            type="number"
          />
        </div>
      </div>
      <DropDownList
        key="heatingType"
        title="Отопление"
        list={listHeating}
        setValue={(el: number) => setProperty("heatingType", el)}
      />
      <DropDownList
        key="sewerageType"
        title="Канализация"
        list={listSewerage}
        setValue={(el: number) => setProperty("sewerageType", el)}
      />
      <DropDownList
        key="waterType"
        title="Вода"
        list={listWatter}
        setValue={(el: number) => setProperty("waterType", el)}
      />
      <div className="is-buttons">
        <label className=" m-3 label buttons-discription">Наличие лифта </label>
        <button
          className={`button ${!hasElevator && "is-button-active"} `}
          onClick={() => toggleHasElevator()}
        >
          Нет
        </button>
        <button
          className={`button ${hasElevator && "is-button-active"} `}
          onClick={() => toggleHasElevator()}
        >
          Есть
        </button>
      </div>

      <DropDownList
        title="Електричество"
        list={listElectricity}
        setValue={(el: number) => setProperty("electricityType", el)}
      />

      <DropDownList
        title="Этаж"
        list={floorMap}
        setValue={(el: number) => setProperty("floor", el)}
      />
      <DropDownList
        title="Этажность дома"
        list={listTotalBuildingStoreysMap}
        setValue={(el: number) => setProperty("totalBuildingStoreys", el)}
      />
    </div>
  );
};
