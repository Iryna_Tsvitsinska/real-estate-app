import { useState } from "react";
import {
  BasicRealEstateObjectConfig,
  OwnershipType,
  UtilityConnectionType,
} from "../../realEstateObjectInterfaces";
import { DropDownList } from "../DropDownList";

import {
  createParamsList,
  ElectricityTypeMap,
  floorTypeMap,
  HeatingTypeMap,
  OwnershipTypeMap,
  SewerageTypeMap,
  sortArrObj,
  totalBuildingStoreysMap,
  WatterTypeMap,
} from "../PropertyTypeMap";

interface BasicFieldsComponentConfig {
  setProperty(
    name: keyof BasicRealEstateObjectConfig,
    value: number | boolean
  ): void;
  initialProperties: InitialBasicPropertiesConfig;
}

interface InitialBasicPropertiesConfig {
  gasType: UtilityConnectionType;
  waterType: UtilityConnectionType;
  sewerageType: UtilityConnectionType;
  electricityType: UtilityConnectionType;
  heatingType: UtilityConnectionType;
  floor: number;
  numberOfStoreys: number;
  totalBuildingStoreys: number;
  hasElevator: boolean;
  ownershipType: OwnershipType;
  activeSquare: number;
}

export const BasicFieldsComponent = (props: BasicFieldsComponentConfig) => {
  const { setProperty, initialProperties } = props;
  const [hasElevator, setHasElevator] = useState(initialProperties.hasElevator);

  const listOwnership = createParamsList(
    OwnershipTypeMap,
    initialProperties.ownershipType
  );

  const listWatter = createParamsList(
    WatterTypeMap,
    initialProperties.waterType
  );

  const listSewerage = createParamsList(
    SewerageTypeMap,
    initialProperties.sewerageType
  );
  const listHeating = createParamsList(
    HeatingTypeMap,
    initialProperties.heatingType
  );
  const listElectricity = createParamsList(
    ElectricityTypeMap,
    initialProperties.electricityType
  );

  const floorMap = createParamsList(floorTypeMap, initialProperties.floor);
  const listTotalBuildingStoreysMap = createParamsList(
    totalBuildingStoreysMap,
    initialProperties.totalBuildingStoreys
  );

  sortArrObj(floorMap);

  const toggleHasElevator = () => {
    setProperty("hasElevator", !hasElevator);
    setHasElevator((prev) => !prev);
  };

  return (
    <div className="block">
      <DropDownList
        key="heatingType"
        title="Отопление"
        list={listHeating}
        setValue={(el: number) => setProperty("heatingType", el)}
      />
      <DropDownList
        key="sewerageType"
        title="Канализация"
        list={listSewerage}
        setValue={(el: number) => setProperty("sewerageType", el)}
      />
      <DropDownList
        key="waterType"
        title="Вода"
        list={listWatter}
        setValue={(el: number) => setProperty("waterType", el)}
      />
      <DropDownList
        key="ownershipType"
        title="Право собственности"
        list={listOwnership}
        setValue={(el: number) => setProperty("ownershipType", el)}
      />
      <div className="is-buttons">
        <label className=" m-3 label buttons-discription">Наличие лифта </label>
        <button
          className={`button ${!hasElevator && "is-button-active"} `}
          onClick={() => toggleHasElevator()}
        >
          Нет
        </button>
        <button
          className={`button ${hasElevator && "is-button-active"} `}
          onClick={() => toggleHasElevator()}
        >
          Есть
        </button>
      </div>

      <DropDownList
        title="Електричество"
        list={listElectricity}
        setValue={(el: number) => setProperty("electricityType", el)}
      />
      <DropDownList
        title="Право собственности"
        list={listOwnership}
        setValue={(el: number) => setProperty("ownershipType", el)}
      />

      <DropDownList
        title="Этаж"
        list={floorMap}
        setValue={(el: number) => setProperty("floor", el)}
      />
      <DropDownList
        title="Этажность дома"
        list={listTotalBuildingStoreysMap}
        setValue={(el: number) => setProperty("totalBuildingStoreys", el)}
      />
    </div>
  );
};
