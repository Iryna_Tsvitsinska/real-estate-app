import { useState } from "react";
import { HouseConfig } from "../../realEstateObjectInterfaces";

import { BasicFieldsComponent } from "./BasicFieldComponent";

interface HouseFieldsComponentConfig {
  setProperty(name: keyof HouseConfig, value: number | boolean): void;
  objectRealEstate: HouseConfig;
}

export const HouseFieldsComponent = (props: HouseFieldsComponentConfig) => {
  const { setProperty, objectRealEstate } = props;
  const [activeSquare, setActiveSquare] = useState(
    objectRealEstate.activeSquare
  );
  const [gardenSquare, setGardenSquare] = useState(
    objectRealEstate.gardenSquare
  );

  const {
    gasType,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    hasElevator,
    ownershipType,
  } = objectRealEstate;

  const initialBasicProperties = {
    gasType,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    hasElevator,
    ownershipType,
    activeSquare,
  };

  const onChangeActiveSquareHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = Number(e.target.value);
    setActiveSquare(value);
    setProperty("activeSquare", value);
  };
  const onChangeGardenSquareHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = Number(e.target.value);
    setProperty("gardenSquare", value);
    setGardenSquare(value);
  };

  return (
    <div className="block">
      <div className="columns">
        <div className="column">
          <label className="label">Активная площадь</label>
          <input
            className="input"
            type="number"
            name="activeSquare"
            placeholder="0"
            value={activeSquare}
            onChange={onChangeActiveSquareHandler}
          />
        </div>
        <div className="column">
          <label className="label">Площадь сада</label>
          <input
            className="input"
            type="number"
            name="gardenSquare"
            placeholder="0"
            value={gardenSquare}
            onChange={onChangeGardenSquareHandler}
          />
        </div>
      </div>
      <BasicFieldsComponent
        setProperty={setProperty}
        initialProperties={initialBasicProperties}
      />
    </div>
  );
};
