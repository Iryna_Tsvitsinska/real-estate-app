import {
  FlatConfig,
  GarageConfig,
  HouseConfig,
  OfficeConfig,
  OwnershipType,
  RealEstateStateType,
  RealEstateType,
  UtilityConnectionType,
  WarehouseConfig,
} from "../../realEstateObjectInterfaces";

export const initialPropertiesFlat: FlatConfig = {
  fullSquare: 0,
  activeSquare: 0,
  gasType: UtilityConnectionType.CENTRALIZED,
  waterType: UtilityConnectionType.CENTRALIZED,
  sewerageType: UtilityConnectionType.CENTRALIZED,
  electricityType: UtilityConnectionType.CENTRALIZED,
  heatingType: UtilityConnectionType.OWN,
  floor: 0,
  numberOfStoreys: 0,
  totalBuildingStoreys: 0,
  price: 0,
  hasElevator: false,
  ownershipType: OwnershipType.PRIVATE,
  propertyType: RealEstateType.FLAT, // вид
  propertyState: RealEstateStateType.SALE, //тип
  id: 0,
  description: "",
  images: [],
  hasBalcon: false,
};

export const initialPropertiesOffice: OfficeConfig = {
  fullSquare: 0,
  activeSquare: 0,
  gasType: UtilityConnectionType.NONE,
  waterType: UtilityConnectionType.CENTRALIZED,
  sewerageType: UtilityConnectionType.CENTRALIZED,
  electricityType: UtilityConnectionType.CENTRALIZED,
  heatingType: UtilityConnectionType.CENTRALIZED,
  floor: 0,
  numberOfStoreys: 0,
  totalBuildingStoreys: 0,
  price: 0,
  hasElevator: false,
  ownershipType: OwnershipType.PRIVATE,
  propertyType: RealEstateType.OFFICE, // вид
  propertyState: RealEstateStateType.SALE, //тип
  id: 0,
  description: "",
  images: [],
  hasParking: false,
};
export const initialPropertiesGarage: GarageConfig = {
  fullSquare: 0,
  activeSquare: 0,
  gasType: UtilityConnectionType.NONE,
  waterType: UtilityConnectionType.NONE,
  sewerageType: UtilityConnectionType.NONE,
  electricityType: UtilityConnectionType.NONE,
  heatingType: UtilityConnectionType.NONE,
  floor: 0,
  numberOfStoreys: 0,
  totalBuildingStoreys: 1,
  price: 0,
  hasElevator: false,
  ownershipType: OwnershipType.PRIVATE,
  propertyType: RealEstateType.GARAGE, // вид
  propertyState: RealEstateStateType.SALE, //тип
  id: 0,
  description: "",
  images: [],
  hasCellar: false,
  hasServicePit: false,
};
export const initialPropertiesHouse: HouseConfig = {
  fullSquare: 0,
  activeSquare: 0,
  gasType: UtilityConnectionType.NONE,
  waterType: UtilityConnectionType.NONE,
  sewerageType: UtilityConnectionType.NONE,
  electricityType: UtilityConnectionType.NONE,
  heatingType: UtilityConnectionType.NONE,
  floor: 0,
  numberOfStoreys: 0,
  totalBuildingStoreys: 1,
  price: 0,
  hasElevator: false,
  ownershipType: OwnershipType.PRIVATE,
  propertyType: RealEstateType.GARAGE, // вид
  propertyState: RealEstateStateType.SALE, //тип
  id: 0,
  images: [],
  description: "",
  gardenSquare: 0,
};
export const initialPropertiesWarehouse: WarehouseConfig = {
  fullSquare: 0,
  activeSquare: 0,
  gasType: UtilityConnectionType.NONE,
  waterType: UtilityConnectionType.NONE,
  sewerageType: UtilityConnectionType.NONE,
  electricityType: UtilityConnectionType.NONE,
  heatingType: UtilityConnectionType.NONE,
  floor: 0,
  numberOfStoreys: 0,
  totalBuildingStoreys: 1,
  price: 0,
  hasElevator: false,
  ownershipType: OwnershipType.PRIVATE,
  propertyType: RealEstateType.GARAGE, // вид
  propertyState: RealEstateStateType.SALE, //тип
  id: 0,
  images: [],
  description: "",
  hasSecurity: false,
};
