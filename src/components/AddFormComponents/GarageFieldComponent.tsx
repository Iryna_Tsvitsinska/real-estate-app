import { GarageConfig } from "../../realEstateObjectInterfaces";

import { BasicFieldsComponent } from "./BasicFieldComponent";

interface GarageFieldsComponentConfig {
  setProperty(name: keyof GarageConfig, value: number | boolean): void;
  objectRealEstate: GarageConfig;
}

export const GarageFieldsComponent = (props: GarageFieldsComponentConfig) => {
  const { setProperty, objectRealEstate } = props;

  const {
    gasType,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    hasElevator,
    ownershipType,
    activeSquare,
  } = objectRealEstate;

  const initialBasicProperties = {
    gasType,
    waterType,
    sewerageType,
    electricityType,
    heatingType,
    floor,
    numberOfStoreys,
    totalBuildingStoreys,
    hasElevator,
    ownershipType,
    activeSquare,
  };

  return (
    <div className="block">
      <BasicFieldsComponent
        setProperty={setProperty}
        initialProperties={initialBasicProperties}
      />
    </div>
  );
};
