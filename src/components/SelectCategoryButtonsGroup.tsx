import {
  ActiveCategories,
  RealEstateType,
} from "../realEstateObjectInterfaces";

import {
  SelectCategoryButton,
  SelectCategoryButtonType,
} from "./SelectCategoryButton";

interface SelectedCategoryButtonsConfig {
  [index: number]: {
    title: string;
    buttonType: keyof typeof SelectCategoryButtonType;
  };
}
const selectedCategoryButtons: SelectedCategoryButtonsConfig = {
  0: { title: "Гараж", buttonType: "GARAGE" },
  1: { title: "Склад", buttonType: "WAREHOUSE" },
  2: { title: "Офис", buttonType: "OFFICE" },
  3: { title: "Квартира", buttonType: "FLAT" },
  4: { title: "Дом", buttonType: "HOUSE" },
};

interface SelectCategoryButtonsGroupGonfig {
  buttonsId: number[];
  activeCategories: ActiveCategories;
  onChange(id: RealEstateType, isSelected: boolean): void;
}

export const SelectCategoryButtonsGroup = (
  props: SelectCategoryButtonsGroupGonfig
) => {
  const { buttonsId, activeCategories, onChange } = props;

  return (
    <div className="is-flex is-justify-content-center">
      {buttonsId
        .filter((id) => selectedCategoryButtons[id])
        .map((id) => (
          <div key={id} className="m-1">
            <SelectCategoryButton
              label={selectedCategoryButtons[id].title}
              type={selectedCategoryButtons[id].buttonType}
              isSelected={activeCategories.includes(id)}
              onChange={(isSelected) => onChange(id, isSelected)}
            />
          </div>
        ))}
    </div>
  );
};
