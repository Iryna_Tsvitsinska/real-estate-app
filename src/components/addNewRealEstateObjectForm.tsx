import React, { useState } from "react";
import { hideAddForm } from "../redux/addRealEstateObjectSlice";
import {
  setRealEstateObjects,
  setSearchLimits,
} from "../redux/realEstateListSlice";

import { useAppDispatch } from "../redux/store";

import "../scss/AddFormStyle.scss";
import { GarageFieldsComponent } from "./AddFormComponents/GarageFieldComponent";
import { FlatFieldsComponent } from "./AddFormComponents/FlatFieldComponent";
import { ServerAPI } from "../ServerAPI";
import {
  FlatConfig,
  GarageConfig,
  HouseConfig,
  OfficeConfig,
  RealEstateObjectTypeConfig,
  RealEstateStateType,
  RealEstateType,
  WarehouseConfig,
} from "../realEstateObjectInterfaces";
import { OfficeFieldsComponent } from "./AddFormComponents/OfficeFieldComponent";
import {
  initialPropertiesFlat,
  initialPropertiesGarage,
  initialPropertiesHouse,
  initialPropertiesOffice,
  initialPropertiesWarehouse,
} from "./AddFormComponents/initialRealEstateObjectData";
import { HouseFieldsComponent } from "./AddFormComponents/HouseFieldComponent";
import { WarehouseFieldsComponent } from "./AddFormComponents/WarehouseFieldComponent";

import { AddImagesForm } from "./image/AddImagesForm";
import { useNavigate } from "react-router";

const AddFormNewRealEstateObject = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [type, setType] = useState<RealEstateType>(RealEstateType.FLAT);

  const [newRealEstateObject, setRealEstateObject] =
    useState<RealEstateObjectTypeConfig>({ ...initialPropertiesFlat });

  const setPropertyToStateComponent = <
    K extends keyof RealEstateObjectTypeConfig
  >(
    name: K,
    value: RealEstateObjectTypeConfig[K]
  ) => {
    setRealEstateObject((prev: RealEstateObjectTypeConfig) => ({
      ...prev,
      [name]: value,
    }));
  };
  const addImagesToStateComponent = (file: File) => {
    const result = ServerAPI.addImageObjectRealEstate(
      file,
      newRealEstateObject.id
    );

    result.then((data) => {
      setRealEstateObject((prev: RealEstateObjectTypeConfig) => ({
        ...prev,
        images: [...prev.images, data],
      }));
    });
  };
  const delImagesToStateComponent = (id: number) => {
    setRealEstateObject((prev: RealEstateObjectTypeConfig) => ({
      ...prev,
      images: prev.images.filter((el) => el.imageId !== id),
    }));
  };

  const setTypeHandler = (name: RealEstateType) => {
    if (name === RealEstateType.FLAT) {
      setRealEstateObject(initialPropertiesFlat);
    } else if (name === RealEstateType.OFFICE) {
      setRealEstateObject(initialPropertiesOffice);
    } else if (name === RealEstateType.GARAGE) {
      setRealEstateObject(initialPropertiesGarage);
    } else if (name === RealEstateType.WAREHOUSE) {
      setRealEstateObject(initialPropertiesWarehouse);
    } else if (name === RealEstateType.HOUSE) {
      setRealEstateObject(initialPropertiesHouse);
    }
    setType(name);
  };

  const onChangeValueHandle = (e: React.ChangeEvent<HTMLInputElement>) => {
    var regExp = /[1-9][0-9]*?$/;
    const result = e.target.value.match(regExp);
    const isNull = e.target.value === "0" || e.target.value === "";
    const value = Number(result);
    const name = e.target.name;
    (result || isNull) &&
      setRealEstateObject((prev) => ({
        ...prev,
        [name]: isNull ? 0 : value,
      }));
  };

  const onChangeDescriptionHandler = (
    e: React.FormEvent<HTMLTextAreaElement>
  ) => {
    const value = e.currentTarget.value;
    setRealEstateObject((prev) => ({
      ...prev,
      description: value,
    }));
  };

  const addObjectRealEstateHandler = () => {
    const result = ServerAPI.addNewObjectRealEstate(newRealEstateObject);

    result.then((data) => {
      dispatch(setRealEstateObjects(data.currentList));
      dispatch(setSearchLimits(data.totalLimits));
      closeAddFormHandler();
    });
  };
  const closeAddFormHandler = () => {
    dispatch(hideAddForm());
    navigate("/", { replace: true });
  };

  return (
    <div className={"box"}>
      <section className="block">
        <p className="title">Новый обьект недвижимости</p>
        <div>
          <label className="label">Я хочу: </label>
          <button
            className={`button ${
              newRealEstateObject.propertyState === RealEstateStateType.SALE &&
              "is-button-active"
            }`}
            onClick={() =>
              setPropertyToStateComponent(
                "propertyState",
                RealEstateStateType.SALE
              )
            }
          >
            Продать
          </button>
          <button
            className={`button ${
              newRealEstateObject.propertyState === RealEstateStateType.RENT &&
              "is-button-active"
            }`}
            onClick={() =>
              setPropertyToStateComponent(
                "propertyState",
                RealEstateStateType.RENT
              )
            }
          >
            Сдать в оренду
          </button>
        </div>
        <label className="label">Выберите вид недвижимости</label>

        <div className="m-3 is-buttons">
          <button
            className={`button ${
              type === RealEstateType.GARAGE && "is-button-active"
            } `}
            onClick={() => setTypeHandler(RealEstateType.GARAGE)}
          >
            Гараж
          </button>
          <button
            className={`button ${
              type === RealEstateType.WAREHOUSE && "is-button-active"
            } `}
            onClick={() => setTypeHandler(RealEstateType.WAREHOUSE)}
          >
            Склад
          </button>
          <button
            className={`button ${
              type === RealEstateType.OFFICE && "is-button-active"
            } `}
            onClick={() => setTypeHandler(RealEstateType.OFFICE)}
          >
            Офис
          </button>

          <button
            className={`button ${
              type === RealEstateType.FLAT && "is-button-active"
            } `}
            onClick={() => setTypeHandler(RealEstateType.FLAT)}
          >
            Квартира
          </button>
          <button
            className={`button ${
              type === RealEstateType.HOUSE && "is-button-active"
            } `}
            onClick={() => setTypeHandler(RealEstateType.HOUSE)}
          >
            Дом
          </button>
        </div>
        <div className="columns">
          <div className="column">
            <label className="label">Цена</label>
            <input
              className="input"
              name="price"
              type="number"
              placeholder="0"
              value={
                newRealEstateObject.price !== 0 ? newRealEstateObject.price : ""
              }
              onChange={onChangeValueHandle}
            />
          </div>
          <div className="column">
            <label className="label">Полная площадь</label>
            <input
              className="input"
              type="number"
              name="fullSquare"
              placeholder="0"
              value={
                newRealEstateObject.fullSquare !== 0
                  ? newRealEstateObject.fullSquare
                  : ""
              }
              onChange={onChangeValueHandle}
            />
          </div>
        </div>
        {type === RealEstateType.GARAGE && (
          <GarageFieldsComponent
            setProperty={setPropertyToStateComponent}
            objectRealEstate={newRealEstateObject as GarageConfig}
          />
        )}
        {type === RealEstateType.FLAT && (
          <FlatFieldsComponent
            setProperty={setPropertyToStateComponent}
            objectRealEstate={newRealEstateObject as FlatConfig}
          />
        )}
        {type === RealEstateType.OFFICE && (
          <OfficeFieldsComponent
            setProperty={setPropertyToStateComponent}
            objectRealEstate={newRealEstateObject as OfficeConfig}
          />
        )}
        {type === RealEstateType.HOUSE && (
          <HouseFieldsComponent
            setProperty={setPropertyToStateComponent}
            objectRealEstate={newRealEstateObject as HouseConfig}
          />
        )}
        {type === RealEstateType.WAREHOUSE && (
          <WarehouseFieldsComponent
            setProperty={setPropertyToStateComponent}
            objectRealEstate={newRealEstateObject as WarehouseConfig}
          />
        )}

        <AddImagesForm
          images={newRealEstateObject.images}
          addImage={addImagesToStateComponent}
          delImage={delImagesToStateComponent}
        />
        <label className="label">Описание обьекта недвижимости</label>
        <textarea
          className="discription-fild"
          name="discription"
          placeholder="Описание обьекта недвижимости"
          onChange={onChangeDescriptionHandler}
        ></textarea>
      </section>
      <div className="modal-card-foot">
        <button
          className={"button"}
          disabled={!newRealEstateObject.description.trim()}
          onClick={addObjectRealEstateHandler}
        >
          Добавить обьект
        </button>
      </div>
    </div>
  );
};

export default AddFormNewRealEstateObject;
