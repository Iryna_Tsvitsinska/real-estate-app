import { ServerAPI } from "../ServerAPI";
import { setInitialState } from "../redux/realEstateListSlice";
import { useAppDispatch } from "../redux/store";
import { useEffect } from "react";

export const InitialDataFetcher = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    ServerAPI.getInitialData().then((data) => {
      dispatch(setInitialState(data));
    });
  }, [dispatch]);

  return null;
};
