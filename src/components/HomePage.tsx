import { List } from "./list";
import ListFetcher from "./ListFetcher";
import SearchForm from "./SearchForm";

export const HomePage = () => {
  return (
    <div>
      <ListFetcher />
      <SearchForm />
      <List />
    </div>
  );
};
