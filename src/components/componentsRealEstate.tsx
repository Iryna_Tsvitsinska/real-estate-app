import {
  GarageConfig,
  HouseConfig,
  OfficeConfig,
  WarehouseConfig,
} from "../realEstateObjectInterfaces";

export const renderGarage = (garage: GarageConfig) => {
  return (
    <div key={garage.id} className="box columns m-3 is-vcentered">
      <div className="column is-2">
        <div className="icon is-large">
          <i className="fas fa-2x fa-warehouse"></i>
        </div>
      </div>
      <div className="column">
        <div className="subtitle">{`Гараж  Площадь: ${garage.fullSquare}м.кв Цена: ${garage.price}$`}</div>
        {garage.hasServicePit && (
          <button className="button is-info is-outlined">
            {"has Service Pit"}
          </button>
        )}
      </div>
    </div>
  );
};

export const renderWareHouse = (warehouse: WarehouseConfig) => {
  return (
    <div key={warehouse.id} className="box columns m-3 is-vcentered">
      <div className="column is-2">
        <div className="icon is-large">
          <i className="fas fa-2x fa-boxes"></i>
        </div>
      </div>
      <div className="column">
        <div className="subtitle">{`Склад  Площадь: ${warehouse.fullSquare}м.кв. Цена: ${warehouse.price}$`}</div>
        {warehouse.hasSecurity && (
          <button className="button is-info is-outlined">
            {"has Secirity"}
          </button>
        )}
      </div>
    </div>
  );
};
export const renderOffice = (office: OfficeConfig) => {
  return (
    <div key={office.id} className="box columns m-3 is-vcentered">
      <div className="column is-2">
        <div className="icon is-large">
          <i className="fas fa-2x fa-laptop-house"></i>
        </div>
      </div>
      <div className="column">
        <div className="subtitle">{`Офис  Площадь: ${office.fullSquare}м.кв. Цена: ${office.price}$`}</div>
      </div>
    </div>
  );
};

// export const renderFlat = (flat: FlatConfig) => {
//   // const dispatch = useAppDispatch();
//   return (
//     <div key={flat.id} className="box columns m-3 is-vcentered">
//       <div className="column is-2">
//         <div className="icon is-large">
//           <button
//             className="button" /*onClick={() => dispatch(showEditForm())}*/
//           >
//             <i className="fas fa-2x fa-building"></i>
//           </button>
//         </div>
//       </div>
//       <div className="column">
//         <div className="subtitle">{`Квартира  Площадь: ${flat.fullSquare}/ ${flat.activeSquare}м.кв. Цена: ${flat.price}$`}</div>
//         <div className="is-flex is-justify-content-center">
//           {flat.hasBalcon && (
//             <button className="button is-info is-outlined">{"Balcon"}</button>
//           )}
//           {flat.heatingType === UtilityConnectionType.OWN && (
//             <button className="button is-info is-outlined">
//               {"Own heating"}
//             </button>
//           )}
//         </div>
//       </div>
//     </div>
//   );
// };
export const renderHouse = (house: HouseConfig) => {
  return (
    <div key={house.id} className="box columns m-3 is-vcentered">
      <div className="column is-2">
        <div className="icon is-large">
          <i className="fas fa-2x fa-home"></i>
        </div>
      </div>
      <div className="column">
        <div className="subtitle">{`Дом  Площадь: ${house.fullSquare}м.кв. Цена: ${house.price}$`}</div>
      </div>
    </div>
  );
};
