import { RealEstateObjectTypeConfig } from "../realEstateObjectInterfaces";

export const getMaxValuePrice = (list: RealEstateObjectTypeConfig[]) => {
  const maxElem = list.reduce((res, current) =>
    res.price > current.price ? res : current
  );
  return maxElem.price;
};
export const getMaxValueSquare = (list: RealEstateObjectTypeConfig[]) => {
  const maxElem = list.reduce((res, current) =>
    res.fullSquare > current.fullSquare ? res : current
  );
  return maxElem.fullSquare;
};
export const getMinValueSquare = (list: RealEstateObjectTypeConfig[]) => {
  const maxElem = list.reduce((res, current) =>
    res.fullSquare < current.fullSquare ? res : current
  );
  return maxElem.fullSquare;
};
export const getMinValuePrice = (list: RealEstateObjectTypeConfig[]) => {
  const maxElem = list.reduce((res, current) =>
    res.price < current.price ? res : current
  );
  return maxElem.price;
};

export const getnewIdValue = (list: RealEstateObjectTypeConfig[]) => {
  return list.reduce((maxId, item) => Math.max(maxId, item.id), 0) + 1;
};
