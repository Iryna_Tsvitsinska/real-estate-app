import { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import {
  FlatConfig,
  GarageConfig,
  HouseConfig,
  OfficeConfig,
  RealEstateObjectTypeConfig,
  RealEstateType,
  WarehouseConfig,
} from "../realEstateObjectInterfaces";
import {
  setRealEstateObjects,
  setSearchLimits,
} from "../redux/realEstateListSlice";
import { hideEditForm } from "../redux/editObjectRealEstateSlice";
import { RootState, useAppDispatch } from "../redux/store";
import { ServerAPI } from "../ServerAPI";
import { FlatFieldsComponent } from "./AddFormComponents/FlatFieldComponent";
import { GarageFieldsComponent } from "./AddFormComponents/GarageFieldComponent";
import { HouseFieldsComponent } from "./AddFormComponents/HouseFieldComponent";
import { OfficeFieldsComponent } from "./AddFormComponents/OfficeFieldComponent";
import { WarehouseFieldsComponent } from "./AddFormComponents/WarehouseFieldComponent";
import { AddImagesForm } from "./image/AddImagesForm";
import { useNavigate } from "react-router";

export type EditFormProps = ConnectedProps<typeof connector>;

const EditForm = (props: EditFormProps) => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { editingObjectId } = props;

  const [realEstateObject, setRealEstateObject] =
    useState<RealEstateObjectTypeConfig>();

  useEffect(() => {
    ServerAPI.editRealEstateObject(editingObjectId).then((data) => {
      setRealEstateObject(data);
    });
  }, [editingObjectId]);

  const setPropertyToStateComponent = <
    K extends keyof RealEstateObjectTypeConfig
  >(
    name: K,
    value: RealEstateObjectTypeConfig[K]
  ) => {
    setRealEstateObject((prev) =>
      prev
        ? {
            ...prev,
            [name]: value,
          }
        : undefined
    );
  };

  if (!realEstateObject) {
    return <div>"loading"</div>;
  }

  const onChangeValueHandle = (e: React.ChangeEvent<HTMLInputElement>) => {
    var regExp = /^[^0]*[0-9]+$/;
    const tValue = e.target.value;

    if (e.target.value.match(regExp)) {
    } else {
      console.log("n", tValue.match(regExp));
    }
    console.log(tValue.match(regExp));
    const name = e.target.name;
    const value = Number(e.target.value);

    value &&
      setRealEstateObject((prev) =>
        prev
          ? {
              ...prev,
              [name]: value,
            }
          : undefined
      );
  };
  const addImagesHandler = (file: File) => {
    const result = ServerAPI.addImageObjectRealEstate(
      file,
      realEstateObject.id
    );

    result.then((data) => {
      setRealEstateObject((prev) =>
        prev
          ? {
              ...prev,
              images: [...prev.images, data],
            }
          : undefined
      );
    });
  };
  const delImagesHandler = (imageId: number) => {
    const result = ServerAPI.deleteImage(imageId);

    result.then((data) => console.log(data));
    setRealEstateObject((prev) =>
      prev
        ? {
            ...prev,
            images: prev.images.filter((el) => el.imageId !== imageId),
          }
        : undefined
    );
  };

  const onChangeDescriptionHandler = (
    e: React.FormEvent<HTMLTextAreaElement>
  ) => {
    const value = e.currentTarget.value;

    setRealEstateObject((prev) =>
      prev
        ? {
            ...prev,
            description: value,
          }
        : undefined
    );
  };

  const deleteObjectRealEstateHandler = () => {
    const result = ServerAPI.deleteObjectRealEstate(realEstateObject.id);

    result.then((data) => {
      dispatch(setRealEstateObjects(data.currentList));
      dispatch(setSearchLimits(data.totalLimits));
      dispatch(hideEditForm());
    });
  };

  const saveObjectRealEstateHandler = () => {
    const result = ServerAPI.saveChangesObjectRealEstate(realEstateObject);
    result.then((data) => {
      dispatch(setRealEstateObjects(data.currentList));
      dispatch(setSearchLimits(data.totalLimits));
      dispatch(hideEditForm());
      navigate("/", { replace: true });
    });
  };

  return (
    <div className="box">
      <section className="modal-card-body">
        <div className="columns">
          <div className="column">
            <label className="label">Цена</label>
            <input
              className="input"
              name="price"
              type="number"
              placeholder="0"
              value={realEstateObject.price}
              onChange={onChangeValueHandle}
            />
          </div>
          <div className="column">
            <label className="label">Полная площадь</label>
            <input
              className="input"
              type="number"
              name="fullSquare"
              placeholder="0"
              value={realEstateObject.fullSquare}
              onChange={onChangeValueHandle}
            />
          </div>
        </div>
        {realEstateObject.propertyType === RealEstateType.GARAGE && (
          <GarageFieldsComponent
            setProperty={setPropertyToStateComponent}
            objectRealEstate={realEstateObject as GarageConfig}
          />
        )}
        {realEstateObject.propertyType === RealEstateType.FLAT && (
          <FlatFieldsComponent
            setProperty={setPropertyToStateComponent.bind(this)}
            objectRealEstate={realEstateObject as FlatConfig}
          />
        )}
        {realEstateObject.propertyType === RealEstateType.OFFICE && (
          <OfficeFieldsComponent
            setProperty={setPropertyToStateComponent.bind(this)}
            objectRealEstate={realEstateObject as OfficeConfig}
          />
        )}
        {realEstateObject.propertyType === RealEstateType.HOUSE && (
          <HouseFieldsComponent
            setProperty={setPropertyToStateComponent.bind(this)}
            objectRealEstate={realEstateObject as HouseConfig}
          />
        )}
        {realEstateObject.propertyType === RealEstateType.WAREHOUSE && (
          <WarehouseFieldsComponent
            setProperty={setPropertyToStateComponent}
            objectRealEstate={realEstateObject as WarehouseConfig}
          />
        )}

        <AddImagesForm
          images={realEstateObject.images}
          addImage={addImagesHandler}
          delImage={delImagesHandler}
        />

        <label className="label">Описание обьекта недвижимости</label>
        <textarea
          className="discription-fild"
          name="discription"
          placeholder="Описание обьекта недвижимости"
          onChange={onChangeDescriptionHandler}
          value={realEstateObject.description}
        ></textarea>
      </section>
      <div className="modal-card-foot">
        <button
          className={"button is-success"}
          onClick={saveObjectRealEstateHandler}
        >
          Сохранить
        </button>
        <button
          className={"button is-danger "}
          onClick={deleteObjectRealEstateHandler}
        >
          удалить
        </button>
      </div>
    </div>
  );
};

const connector = connect((state: RootState) => ({
  editingObjectId: state.editForm.editingRealEstateObjectId,
}));

export default connector(EditForm);
