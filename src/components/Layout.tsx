import { HeaderRealEstate } from "./header";
import { Outlet } from "react-router-dom";
import { InitialDataFetcher } from "./initialDataFetcher";

export const Layout = () => {
  return (
    <>
      <HeaderRealEstate />
      <InitialDataFetcher />
      <Outlet />
    </>
  );
};
