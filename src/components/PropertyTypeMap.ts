import {
  OwnershipType,
  UtilityConnectionType,
} from "../realEstateObjectInterfaces";

interface ListElem {
  name: string;
  value: number;
  isSelected: boolean;
}

export const OwnershipTypeMap = {
  [OwnershipType.LEGAL]: "Комерческая",
  [OwnershipType.PRIVATE]: "Частная",
  [OwnershipType.RENT]: "В оренде",
  [OwnershipType.MUNICIPAL]: "Муниципальная",
};

export const WatterTypeMap = {
  [UtilityConnectionType.NONE]: "Нет",
  [UtilityConnectionType.CENTRALIZED]: "Централизированная",
  [UtilityConnectionType.OWN]: "Cкважина",
};

export const SewerageTypeMap = {
  [UtilityConnectionType.NONE]: "Нет",
  [UtilityConnectionType.CENTRALIZED]: "Централизированная",
  [UtilityConnectionType.OWN]: "Яма",
};
export const HeatingTypeMap = {
  [UtilityConnectionType.NONE]: "Нет",
  [UtilityConnectionType.CENTRALIZED]: "Централизированное",
  [UtilityConnectionType.OWN]: "Индивидуальное",
};

export const ElectricityTypeMap = {
  [UtilityConnectionType.NONE]: "Нет",
  [UtilityConnectionType.CENTRALIZED]: "Централизированное",
  [UtilityConnectionType.OWN]: "Собственное",
};

export const floorTypeMap = {
  [-1]: "Подвал",
  0: "Цоколь",
  1: "1",
  2: "2",
  3: "3",
  4: "4",
  5: "5",
  6: "6",
  7: "7",
  8: "8",
  9: "9",
};
export const totalBuildingStoreysMap = {
  1: "1",
  2: "2",
  3: "3",
  4: "4",
  5: "5",
  6: "6",
  7: "7",
  8: "8",
  9: "9",
};

export const createParamsList = (
  typeMap: { [name: number]: string },
  selectedElem: number
): ListElem[] => {
  return [...Object.keys(typeMap)].map(Number).map((key) => ({
    name: typeMap[key],
    value: key,
    isSelected: key === selectedElem,
  }));
};
export const sortArrObj = (arr: ListElem[]) => {
  return arr.sort((a: ListElem, b: ListElem) => (a.value > b.value ? 1 : -1));
};
