import "../../src/scss/SelectCategoryButton.scss";

export enum SelectCategoryButtonType {
  GARAGE = "fas fa fa-warehouse",
  WAREHOUSE = "fas fa fa-boxes",
  OFFICE = "fas fa fa-laptop-house",
  FLAT = "fas fa fa-building",
  HOUSE = "fas fa fa-home",
}
interface SelectCategoryButtonConfig {
  label: string;
  type: keyof typeof SelectCategoryButtonType;
  onChange(isSelected: boolean): void;
  isSelected: boolean;
}

export const SelectCategoryButton = (props: SelectCategoryButtonConfig) => {
  const { label, type, onChange, isSelected } = props;

  const isSelectedHandler = () => {
    const newValue = !isSelected;

    onChange(newValue);
  };

  return (
    <button
      className={`button ${
        isSelected && "is-button-active has-background-warning-light"
      }`}
      onClick={isSelectedHandler}
    >
      <span className="icon-text">
        <span className="icon is-vcentered">
          <i className={SelectCategoryButtonType[type]}></i>
        </span>
        <span>{label}</span>
      </span>
    </button>
  );
};
