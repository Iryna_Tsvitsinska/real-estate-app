import { useEffect } from "react";
import { connect } from "react-redux";
import { ActiveCategories } from "../realEstateObjectInterfaces";
import {
  isNeedRequestData,
  setRealEstateObjects,
  setSearchLimits,
} from "../redux/realEstateListSlice";
import { RootState, useAppDispatch } from "../redux/store";
import { ServerAPI } from "../ServerAPI";
interface ListFetcherPropsConfig {
  activeCategories: ActiveCategories;
  minPriceUser: number;
  maxPriceUser: number;
  minSquareUser: number;
  maxSquareUser: number;
  needRequestData: boolean;
}

const ListFetcher = (props: ListFetcherPropsConfig) => {
  const {
    activeCategories,
    minPriceUser,
    maxPriceUser,
    minSquareUser,
    maxSquareUser,
    needRequestData,
  } = props;
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (needRequestData) {
      console.log("ServerAPI.sortRealEstateObjects");
      ServerAPI.sortRealEstateObjects({
        minSquareUser,
        maxSquareUser,
        minPriceUser,
        maxPriceUser,
        activeCategories,
      }).then((data) => {
        dispatch(setRealEstateObjects(data.sortList));
        dispatch(setSearchLimits(data.searchLimits));
        dispatch(isNeedRequestData(false));
      });
    }
  }, [
    dispatch,
    activeCategories,
    minPriceUser,
    maxPriceUser,
    minSquareUser,
    maxSquareUser,
    needRequestData,
  ]);

  return null;
};

const connector = connect((state: RootState) => ({
  activeCategories: state.realEstateList.activeCategories,
  minPriceUser: state.realEstateList.userLimits.minPrice,
  maxPriceUser: state.realEstateList.userLimits.maxPrice,
  minSquareUser: state.realEstateList.userLimits.minSquare,
  maxSquareUser: state.realEstateList.userLimits.maxSquare,
  needRequestData: state.realEstateList.needRequestData,
}));

export default connector(ListFetcher);
