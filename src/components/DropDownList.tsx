import { useState } from "react";

interface ListElem {
  name: string;
  value: number;
  isSelected: boolean;
}

interface DropDownListPropsConfig {
  title: string;
  list: ListElem[];
  setValue(elem: number): void;
}

export const DropDownList = (props: DropDownListPropsConfig) => {
  const { title, list, setValue } = props;
  const initialElem = list.find((elem: ListElem) => elem.isSelected) || list[0];
  const [selectedElem, setSelectedElem] = useState(initialElem);

  const setElemHandler = (e: React.FormEvent<HTMLSelectElement>) => {
    const index = e.currentTarget.selectedIndex;

    let newList = list.map((elem) => ({ ...elem, isSelected: false }));
    setSelectedElem({
      ...newList[index],
      isSelected: true,
    });

    setValue(list[index].value);
  };

  return (
    <div>
      <label className="m-3 label buttons-discription">{title}</label>
      <div className="select is-info">
        <select onChange={setElemHandler} value={selectedElem.value}>
          {list.map((item, index) => (
            <option
              key={index}
              value={item.value} /*selected={item.isSelected}*/
            >
              {item.name}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};
