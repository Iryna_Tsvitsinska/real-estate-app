import React, { useEffect, useRef, useState } from "react";
import "../../src/scss/RangeSelector.scss";

interface RangesSelectorConfig {
  label: string;
  min: number;
  max: number;
  showInputs?: boolean;
  onMinChange(min: number): void;
  onMaxChange(max: number): void;
}

export const RangeSelector = (props: RangesSelectorConfig) => {
  const {
    label,
    min,
    max,
    onMaxChange,
    onMinChange,
    showInputs = true,
  } = props;
  const rectRef = useRef<HTMLDivElement>(null);

  const [leftPinState, setLeftPinState] = useState({
    dragging: false,
    sliderPos: 0,
    sliderValue: 0,
    isTargetInput: false,
    valueInput: 0,
    startMoving: 0,
  });

  const [rightPinState, setRightPinState] = useState({
    dragging: false,
    sliderPos: 0,
    sliderValue: 0,
    isTargetInput: false,
    valueInput: 0,
    startMoving: 0,
  });
  useEffect(() => {
    document.addEventListener("mousemove", onMouseMove);
    document.addEventListener("mouseup", onMouseUp);

    return () => {
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("mouseup", onMouseUp);
    };
  });

  useEffect(() => {
    setLeftPinState((prev) => ({
      ...prev,
      sliderPos: 0,
      sliderValue: min,
      valueInput: min,
    }));
    setRightPinState((prev) => ({
      ...prev,
      sliderPos: 100,
      sliderValue: max,
      valueInput: max,
    }));
  }, [min, max]);

  const onMouseDownLeft = (event: React.MouseEvent<HTMLDivElement>) => {
    const { clientX } = event;

    setLeftPinState((prev) => ({
      ...prev,
      dragging: true,
      startMoving: clientX,
    }));
  };

  const onMouseDownRight = (event: React.MouseEvent<HTMLDivElement>) => {
    const { clientX } = event;
    setRightPinState((prev) => ({
      ...prev,
      dragging: true,
      startMoving: clientX,
    }));
  };

  const getValueByPercentage = (percentage: number) => {
    return Math.round((max - min) * percentage) + min;
  };

  const onMouseMove = (event: MouseEvent) => {
    if (!leftPinState.dragging && !rightPinState.dragging) {
      return;
    }

    const offset = 10;

    const elementRect = rectRef.current!.getBoundingClientRect();
    if (leftPinState.dragging && elementRect) {
      const { clientX } = event;
      const offsetBtn = leftPinState.startMoving - clientX;
      const newX = leftPinState.startMoving - offsetBtn;

      const leftX = elementRect.x;
      const rightX =
        elementRect.x + (rightPinState.sliderPos / 100) * elementRect.width;
      const newPosX = Math.max(Math.min(rightX, newX + offset), leftX);
      const position = (newPosX - elementRect.x) / elementRect.width;
      const sliderPos = position * 100;
      const sliderValue = Math.round(getValueByPercentage(position));
      setLeftPinState((prev) => ({
        ...prev,
        sliderPos,
        sliderValue,
      }));
    }
    if (rightPinState.dragging && elementRect) {
      const { clientX } = event;
      const offsetBtn = leftPinState.startMoving - clientX;
      const newX = leftPinState.startMoving - offsetBtn;
      const leftX =
        elementRect.x + (leftPinState.sliderPos / 100) * elementRect.width;
      const rightX = elementRect.x + elementRect.width;
      const newPosX = Math.max(Math.min(rightX, newX - offset), leftX);
      const position = (newPosX - elementRect.x) / elementRect.width;
      const sliderPos = position * 100;

      const sliderValue = Math.round(getValueByPercentage(position));
      setRightPinState((prev) => ({
        ...prev,
        sliderPos,
        sliderValue,
      }));
    }
  };

  const onMouseUp = (event: MouseEvent) => {
    if (leftPinState.dragging) {
      setLeftPinState((prev) => ({
        ...prev,
        dragging: false,
      }));
      onMinChange(leftPinState.sliderValue);
    }
    if (rightPinState.dragging) {
      setRightPinState((prev) => ({
        ...prev,
        dragging: false,
      }));
      onMaxChange(rightPinState.sliderValue);
    }
  };

  const handleChangeMinPrice = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = Number(event.target.value);
    const elementRect = rectRef.current!.getBoundingClientRect();

    if (elementRect) {
      const newX =
        elementRect.x + (elementRect.width * (value - min)) / (max - min);

      const leftX = elementRect.x;
      const rightX =
        elementRect.x + (rightPinState.sliderPos / 100) * elementRect.width;
      const newPosX = Math.max(Math.min(rightX, newX), leftX);
      const position = (newPosX - elementRect.x) / elementRect.width;
      const sliderPos = position * 100;
      const sliderValue = getValueByPercentage(position);

      value < min
        ? setLeftPinState((prev) => ({
            ...prev,
            isTargetInput: true,
            valueInput: value,
          }))
        : setLeftPinState((prev) => ({
            ...prev,
            sliderPos,
            sliderValue,
            valueInput: value,
            isTargetInput: false,
          }));

      onMinChange(value);
    }
  };

  const handleChangeMaxPrice = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = Number(event.target.value);
    const elementRect = rectRef.current!.getBoundingClientRect();
    if (elementRect) {
      const newX =
        elementRect.x + (elementRect.width * (value - min)) / (max - min);
      const leftX =
        elementRect.x + (leftPinState.sliderPos / 100) * elementRect.width;
      const rightX = elementRect.x + elementRect.width;
      const newPosX = Math.max(Math.min(rightX, newX), leftX);
      const position = (newPosX - elementRect.x) / elementRect.width;
      const sliderPos = position * 100;

      const sliderValue = Math.round(getValueByPercentage(position));

      value < leftPinState.sliderValue
        ? setRightPinState((prev) => ({
            ...prev,
            isTargetInput: true,
            valueInput: value,
          }))
        : setRightPinState((prev) => ({
            ...prev,
            sliderPos,
            sliderValue,
            isTargetInput: false,
          }));

      onMaxChange(value);
    }
  };

  const onBlurMinInput = () => {
    if (leftPinState.valueInput < min) {
      setLeftPinState((prev) => ({
        ...prev,
        sliderPos: 0,
        sliderValue: min,
        isTargetInput: false,
      }));
    }
  };
  const onBlurMaxInput = () => {
    if (rightPinState.valueInput < leftPinState.sliderValue) {
      setRightPinState((prev) => ({
        ...prev,
        sliderPos: leftPinState.sliderPos,
        sliderValue: leftPinState.sliderValue,
        isTargetInput: false,
      }));
    }
  };

  return (
    <div className="range-selector-wrapper">
      <label className="label has-text-centered">{label}:</label>
      {showInputs && (
        <div className="columns">
          <div className="column">
            <div className="is-flex ">
              {/*is-justify-content-flex-end*/}
              <label className="label is-normal "> от</label>
              <p className="control">
                <input
                  className="input "
                  placeholder={`Мин`}
                  name="minPrice"
                  type="number"
                  value={
                    leftPinState.valueInput < leftPinState.sliderValue &&
                    leftPinState.isTargetInput
                      ? leftPinState.valueInput
                      : leftPinState.sliderValue
                  }
                  onChange={handleChangeMinPrice}
                  onBlur={onBlurMinInput}
                ></input>
              </p>
            </div>
          </div>
          <div className="column">
            <div className="is-flex">
              {/*is-justify-content-flex-start */}
              <label className="label is-normal ">до</label>
              <p className="control">
                <input
                  className="input"
                  placeholder={`Max`}
                  name="maxPrice"
                  type="number"
                  value={
                    rightPinState.valueInput < leftPinState.sliderValue &&
                    rightPinState.isTargetInput
                      ? rightPinState.valueInput
                      : rightPinState.sliderValue
                  }
                  onChange={handleChangeMaxPrice}
                  onBlur={onBlurMaxInput}
                ></input>
              </p>
            </div>
          </div>
        </div>
      )}
      <div className="m-5">
        <div
          ref={rectRef}
          className="has-background-grey-lighter range-selector-block"
        >
          <div className="min" style={{ left: `${leftPinState.sliderPos}%` }}>
            <div className="pin-center">
              <div
                onMouseDown={onMouseDownLeft}
                className={`btn ${leftPinState.dragging ? "isDragging" : ""}`}
              ></div>
            </div>
          </div>
          <div className="max" style={{ left: `${rightPinState.sliderPos}%` }}>
            <div className="pin-center">
              <div
                onMouseDown={onMouseDownRight}
                className={`btn ${rightPinState.dragging ? "isDragging" : ""}`}
              ></div>{" "}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
