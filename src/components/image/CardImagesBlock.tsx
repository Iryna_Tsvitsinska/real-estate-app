import { useState } from "react";
import { ServerURL } from "../../axios";
import { ImageInfoConfig } from "../../realEstateObjectInterfaces";
import { AddImageLogo } from "./AddImagesIcon";

import "../../scss/CardImage.scss";
interface CardImagesProps {
  images: ImageInfoConfig[];
  className: string;
}

export const CardImagesBlock = (props: CardImagesProps) => {
  const { images, className } = props;
  const [currentImageIndex, setCurrentImage] = useState(0);
  const amountImages = images ? images.length : 0;

  const nextImageHandler = () => {
    setCurrentImage((prev) => (prev === amountImages ? prev : prev + 1));
  };
  const prevImageHandler = () => {
    setCurrentImage((prev) => (prev === 0 ? prev : prev - 1));
  };

  return (
    <div className="card-image-wrapper">
      <AddImageLogo
        src={
          amountImages !== 0
            ? `${ServerURL}/uploads/${images[currentImageIndex].fileName} `
            : `${ServerURL}/uploads/nofoto.jpg`
        }
        className={`foto-real-estate-container ${className} `}
      />

      {amountImages > 1 && (
        <div className="image-info-bottom">
          <button
            className="button is-small is-pulled-left"
            onClick={prevImageHandler}
            disabled={currentImageIndex === 0}
          >
            <span className="icon">
              <i className="fas fa-2x fa-angle-left"></i>
            </span>
          </button>
          <span>{`${currentImageIndex + 1} / ${amountImages}`}</span>
          <button
            className="button is-small is-pulled-right"
            onClick={nextImageHandler}
            disabled={currentImageIndex === amountImages - 1}
          >
            <span className="icon">
              <i className="fas fa-2x fa-angle-right"></i>
            </span>
          </button>
        </div>
      )}
    </div>
  );
};
