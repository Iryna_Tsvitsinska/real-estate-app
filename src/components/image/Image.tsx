import defoultImage from "../../foto/nofoto.jpg";

type ImageProps = {
  src?: string;
  alt: string;
  className: string;
  width: number;
  height: number;
  delImage(): void;
};

export const Image = (props: ImageProps) => {
  const { src = defoultImage, className, width, height, delImage } = props;

  const styles = {
    backgroundImage: `url(${src})`,
    width: width,
    height: height,
  };

  const delFotoHandler = () => {
    delImage();
  };

  return (
    <div className={`${className} image`} style={styles}>
      <div className="buttons is-right">
        <button className="delete" onClick={delFotoHandler}></button>
      </div>
    </div>
  );
};
