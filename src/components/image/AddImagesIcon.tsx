import { ServerURL } from "../../axios";
import "../../scss/CardImage.scss";

const URLDefaultImage = `${ServerURL}/uploads/nofoto.jpg`;

type ImageProps = {
  src?: string;
  className: string;
};
export const AddImageLogo = (props: ImageProps) => {
  const { src, className } = props;

  const styles = {
    backgroundImage: `url(${src ? src : URLDefaultImage})`,
  };

  return (
    <div className="logo-container">
      <div className={`${className}`} style={styles}></div>
    </div>
  );
};
