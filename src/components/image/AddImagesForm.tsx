import { useState } from "react";
import { ServerURL } from "../../axios";
import { ImageInfoConfig } from "../../realEstateObjectInterfaces";
import { Image } from "./Image";

type AddImagesProps = {
  images: ImageInfoConfig[];
  addImage(file: File): void;
  delImage(value: number): void;
};

export const AddImagesForm = (props: AddImagesProps) => {
  const { images, addImage, delImage } = props;
  const initialValueFileName = "Файл не выбран";
  const [selectedFile, setSelectedFile] =
    useState<string>(initialValueFileName);

  const onFotoSelected = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileList = e.target.files;
    if (!fileList) return;

    addImage(fileList[0]);
    setSelectedFile(fileList[0].name);
  };
  return (
    <>
      <div className="file has-name is-fullwidth m-5">
        <label className="file-label">
          <input
            className="file-input"
            type="file"
            name="avatar"
            onChange={onFotoSelected}
          />
          <span className="file-cta">
            <span className="file-icon">
              <i className="fas fa-upload"></i>
            </span>
            <span className="file-label">Выберите файл…</span>
          </span>
          <span className="file-name">{selectedFile}</span>
        </label>
      </div>
      <div className="columns">
        {images.map((foto, index) => (
          <div className="column" key={index}>
            <Image
              src={`${ServerURL}/uploads/${foto.fileName}`}
              alt="selectImage"
              width={200}
              height={150}
              className="foto-real-estate-container"
              delImage={() => delImage(foto.imageId)}
            />
          </div>
        ))}
      </div>
    </>
  );
};
