import { useNavigate } from "react-router";
import { OfficeConfig } from "../../realEstateObjectInterfaces";
import { showEditForm } from "../../redux/editObjectRealEstateSlice";
import { useAppDispatch } from "../../redux/store";
import { CardImagesBlock } from "../image/CardImagesBlock";

import "../../scss/Card.scss";
interface OfficeInfo {
  office: OfficeConfig;
}

export const OfficeCard = (props: OfficeInfo) => {
  const { office } = props;
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const logoName = office.images[0]?.fileName;

  const editObjectHandler = () => {
    dispatch(showEditForm(office.id));
    navigate("/editRealEstateObject");
  };

  const showObjectHandler = () => {
    navigate(`/show/${office.id}`);
  };

  return (
    <div className="box columns m-3 is-vcentered card-container">
      <div className="p-0 column is-2 logo">
        {logoName ? (
          <CardImagesBlock images={office.images} className="logo-size" />
        ) : (
          <div className="icon-logo is-vertical">
            <i className="fas fa-4x fa-laptop-house"></i>
          </div>
        )}
      </div>
      <div className="column">
        <button
          className="button is-pulled-right"
          onClick={() => editObjectHandler()}
        >
          <i className="far fa-edit"></i>
        </button>
        <div className="is-flex is-justify-content-center is-clickable">
          <div className="card-text" onClick={showObjectHandler}>
            {`Офис  Площадь: ${office.fullSquare}м.кв. Цена: ${office.price}$`}
          </div>
        </div>
      </div>
    </div>
  );
};
