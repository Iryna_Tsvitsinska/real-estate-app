import {
  FlatConfig,
  UtilityConnectionType,
} from "../../realEstateObjectInterfaces";
import { showEditForm } from "../../redux/editObjectRealEstateSlice";
import { useAppDispatch } from "../../redux/store";
import { useNavigate } from "react-router-dom";
import { CardImagesBlock } from "../image/CardImagesBlock";
import "../../scss/Card.scss";

interface FlatCardPropsConfig {
  flat: FlatConfig;
}

export const FlatCard = (props: FlatCardPropsConfig) => {
  const { flat } = props;
  const dispatch = useAppDispatch();
  let navigate = useNavigate();
  const logoName = flat.images[0]?.fileName;

  const editObjectHandler = () => {
    dispatch(showEditForm(flat.id));
    navigate("/editRealEstateObject");
  };
  const showObjectHandler = () => {
    navigate(`/show/${flat.id}`);
  };

  return (
    <div className="box columns m-3 is-vcentered card-container">
      <div className="p-0 column is-2 logo">
        {logoName ? (
          <CardImagesBlock images={flat.images} className="logo-size" />
        ) : (
          <div className="icon-logo">
            <i className="fas fa-4x fa-building"></i>
          </div>
        )}
      </div>
      <div className="column">
        <button
          className="button is-pulled-right mt-5"
          onClick={() => editObjectHandler()}
        >
          <i className="far fa-edit"></i>
        </button>
        <div className="is-flex is-justify-content-center is-clickable">
          <div
            className="card-text"
            onClick={showObjectHandler}
          >{`Квартира  Площадь: ${flat.fullSquare}/ ${flat.activeSquare}м.кв. Цена: ${flat.price}$`}</div>
        </div>
        <div className="is-flex is-justify-content-center">
          {flat.hasBalcon && (
            <button className="button is-info is-outlined is-rounded">
              {"Balcon"}
            </button>
          )}
          {flat.heatingType === UtilityConnectionType.OWN && (
            <button className="button is-info is-outlined is-rounded">
              {"Own heating"}
            </button>
          )}
        </div>
      </div>
    </div>
  );
};
