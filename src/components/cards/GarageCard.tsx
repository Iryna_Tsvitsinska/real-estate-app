import { useNavigate } from "react-router";
import { GarageConfig } from "../../realEstateObjectInterfaces";
import { showEditForm } from "../../redux/editObjectRealEstateSlice";
import { useAppDispatch } from "../../redux/store";
import { CardImagesBlock } from "../image/CardImagesBlock";
import "../../scss/Card.scss";
interface GarageProps {
  garage: GarageConfig;
}

export const GarageCard = (props: GarageProps) => {
  const { garage } = props;
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const logoName = garage.images[0]?.fileName;

  const editObjectHandler = () => {
    dispatch(showEditForm(garage.id));
    navigate("/editRealEstateObject", { replace: true });
  };

  const showObjectHandler = () => {
    navigate(`/show/${garage.id}`);
  };

  return (
    <div
      key={garage.id}
      className="box columns m-3 is-vcentered card-container"
    >
      <div className="p-0 column is-2 logo">
        {logoName ? (
          <CardImagesBlock images={garage.images} className="logo-size" />
        ) : (
          <div className="icon-logo is-vertical">
            <i className="fas fa-4x fa-warehouse"></i>
          </div>
        )}
      </div>
      <div className="column">
        <button
          className="button is-pulled-right"
          onClick={() => editObjectHandler()}
        >
          <i className="far fa-edit"></i>
        </button>
        <div
          className="is-flex is-justify-content-center is-clickable"
          onClick={showObjectHandler}
        >
          <div
            className="card-text"
            onClick={showObjectHandler}
          >{`Гараж  Площадь: ${garage.fullSquare}м.кв. Цена: ${garage.price}$`}</div>
        </div>
        <div className="is-flex is-justify-content-center">
          {garage.hasServicePit && (
            <button className="button is-info is-outlined">
              {"has Service Pit"}
            </button>
          )}
        </div>
      </div>
    </div>
  );
};
