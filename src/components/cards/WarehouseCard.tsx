import { useNavigate } from "react-router";
import { WarehouseConfig } from "../../realEstateObjectInterfaces";
import { showEditForm } from "../../redux/editObjectRealEstateSlice";
import { useAppDispatch } from "../../redux/store";
import { CardImagesBlock } from "../image/CardImagesBlock";
import "../../scss/Card.scss";
interface WarehouseProps {
  warehouse: WarehouseConfig;
}

export const WarehouseCard = (props: WarehouseProps) => {
  const { warehouse } = props;
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const logoName = warehouse.images[0]?.fileName;

  const editObjectHandler = () => {
    dispatch(showEditForm(warehouse.id));
    navigate("/editRealEstateObject");
  };

  const showObjectHandler = () => {
    navigate(`/show/${warehouse.id}`);
  };

  return (
    <div
      key={warehouse.id}
      className="box columns m-3 is-vcentered card-container"
    >
      <div className="p-0 column is-2 logo">
        {logoName ? (
          <CardImagesBlock images={warehouse.images} className="logo-size" />
        ) : (
          <div className="icon-logo is-vertical">
            <i className="fas fa-4x fa-boxes"></i>
          </div>
        )}
      </div>
      <div className="column">
        <button
          className="button is-pulled-right"
          onClick={() => editObjectHandler()}
        >
          <i className="far fa-edit"></i>
        </button>

        <div className="is-flex is-justify-content-center is-clickable">
          <div
            className="card-text"
            onClick={showObjectHandler}
          >{`Склад  Площадь: ${warehouse.fullSquare}м.кв. Цена: ${warehouse.price}$`}</div>
        </div>

        <div className="is-flex is-justify-content-center">
          {warehouse.hasSecurity && (
            <button className="button is-info is-outlined">
              {"has Secirity"}
            </button>
          )}
        </div>
      </div>
    </div>
  );
};
