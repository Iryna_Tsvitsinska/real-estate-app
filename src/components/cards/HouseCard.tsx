import { useNavigate } from "react-router";
import { HouseConfig } from "../../realEstateObjectInterfaces";
import { showEditForm } from "../../redux/editObjectRealEstateSlice";
import { useAppDispatch } from "../../redux/store";
import { CardImagesBlock } from "../image/CardImagesBlock";
import "../../scss/Card.scss";
interface HouseProps {
  house: HouseConfig;
}

export const HouseCard = (props: HouseProps) => {
  const { house } = props;
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const logoName = house.images[0]?.fileName;

  const editObjectHandler = () => {
    dispatch(showEditForm(house.id));

    navigate("/editRealEstateObject", { replace: true });
  };
  const showObjectHandler = () => {
    navigate(`/show/${house.id}`);
  };

  return (
    <div key={house.id} className="box columns m-3 is-vcentered card-container">
      <div className="p-0 column is-2 logo">
        {logoName ? (
          <CardImagesBlock images={house.images} className="logo-size" />
        ) : (
          <div className="icon-logo is-vertical">
            <i className="fas fa-4x fa-home"></i>
          </div>
        )}
      </div>

      <div className="column">
        <button
          className="button is-pulled-right"
          onClick={() => editObjectHandler()}
        >
          <i className="far fa-edit"></i>
        </button>
        <div
          className="is-flex is-justify-content-center is-clickable"
          onClick={showObjectHandler}
        >
          <div className="card-text">{`Дом  Площадь: ${house.fullSquare}/${house.activeSquare}м.кв. Цена: ${house.price}$`}</div>
        </div>
      </div>
    </div>
  );
};
