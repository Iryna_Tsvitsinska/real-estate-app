import { useSelector } from "react-redux";
import {
  FlatConfig,
  GarageConfig,
  HouseConfig,
  OfficeConfig,
  RealEstateObjectTypeConfig,
  RealEstateType,
  WarehouseConfig,
} from "../realEstateObjectInterfaces";
import { RootState } from "../redux/store";
import { FlatCard } from "./cards/FlatCard";
import { GarageCard } from "./cards/GarageCard";
import { HouseCard } from "./cards/HouseCard";
import { OfficeCard } from "./cards/OfficeCard";
import { WarehouseCard } from "./cards/WarehouseCard";

export const List = () => {
  const selectCurrentList = (state: RootState) =>
    state.realEstateList.currentList;
  const currentList = useSelector(selectCurrentList);
  console.log(currentList);

  return (
    <div className="block">
      {currentList.map(
        (realEstateOBject: RealEstateObjectTypeConfig, index: number) => {
          if (realEstateOBject.propertyType === RealEstateType.GARAGE) {
            return (
              <div key={realEstateOBject.id}>
                <GarageCard garage={realEstateOBject as GarageConfig} />
              </div>
            );
          } else if (realEstateOBject.propertyType === RealEstateType.FLAT) {
            return (
              <div key={realEstateOBject.id}>
                <FlatCard flat={realEstateOBject as FlatConfig} />
              </div>
            );
          } else if (
            realEstateOBject.propertyType === RealEstateType.WAREHOUSE
          ) {
            return (
              <div key={realEstateOBject.id}>
                <WarehouseCard
                  warehouse={realEstateOBject as WarehouseConfig}
                />
              </div>
            );
          } else if (realEstateOBject.propertyType === RealEstateType.HOUSE) {
            return (
              <div key={realEstateOBject.id}>
                <HouseCard house={realEstateOBject as HouseConfig} />
              </div>
            );
          } else if (realEstateOBject.propertyType === RealEstateType.OFFICE) {
            return (
              <div key={realEstateOBject.id}>
                <OfficeCard office={realEstateOBject as OfficeConfig} />
              </div>
            );
          }
          return "no data";
        }
      )}
    </div>
  );
};
