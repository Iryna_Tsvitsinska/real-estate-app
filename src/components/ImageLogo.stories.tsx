import React from "react";
import "bulma";
// import "../scss/CardImage.scss";
import { ComponentMeta, ComponentStory } from "@storybook/react";

import { AddImageLogo } from "./image/AddImagesIcon";
import { ServerURL } from "../axios";

export default {
  title: "Image Logo",
  component: AddImageLogo,
  argTypes: {
    src: { control: "object" },
  },
  args: {
    className: "foto-real-estate-container",
  },
} as ComponentMeta<typeof AddImageLogo>;

const Template: ComponentStory<typeof AddImageLogo> = (args) => (
  <div style={{ width: "500px", height: "400px" }}>
    <AddImageLogo {...args} />
  </div>
);

export const Image = Template.bind({});

Image.args = {
  src: `${ServerURL}/uploads/nofoto.jpg`,
  className: "foto-real-estate-container",
};
