// import React, { useState } from "react";
import "./App.css";
import "bulma";
import AddFormNewRealEstateObject from "./components/addNewRealEstateObjectForm";
import EditForm from "./components/EditForm";
import { Routes, Route } from "react-router-dom";
import { HomePage } from "./components/HomePage";
import { Layout } from "./components/Layout";
import { ShowRealEstateObject } from "./components/ShowFormComponents/ShowRealEstateObject";

export default function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<HomePage />} />
          <Route
            path="/addNewRealEstateObject"
            element={<AddFormNewRealEstateObject />}
          />
          <Route path="/editRealEstateObject" element={<EditForm />} />
          <Route path="/show/:id" element={<ShowRealEstateObject />} />
        </Route>
      </Routes>
    </div>
  );
}
